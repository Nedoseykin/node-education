import React from 'react';
import * as PropTypes from 'prop-types';

import List from '../List/List';
import CentralSection from '../CentralSection/CentralSection';

import styles from './AppContent.module.scss';

const AppContent = ({
  filesList,
  activeFileId,
  newCommentValue,
  setActiveFileId,
  setNewComment,
  addNewComment,
  downloadFile,
  comments,
  fileName,
}) => (
  <div className={styles.AppContent}>
    <div
      key="aside"
      className={styles.aside}
    >
      <List
        itemsList={filesList}
        getIsSelected={id => id === activeFileId}
        onItemClick={setActiveFileId}
      />
    </div>
    <div
      key="central"
      className={styles.central}
    >
      <CentralSection
        id={activeFileId}
        title={fileName}
        comments={comments}
        newCommentValue={newCommentValue}
        onNewCommentChange={setNewComment}
        addNewComment={addNewComment}
        downloadFile={downloadFile}
      />
    </div>
  </div>
);

AppContent.propTypes = {
  filesList: PropTypes.arrayOf(PropTypes.shape({})),
  activeFileId: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  comments: PropTypes.shape({}),
  newCommentValue: PropTypes.string,
  fileName: PropTypes.string,
  setActiveFileId: PropTypes.func,
  setNewComment: PropTypes.func,
  addNewComment: PropTypes.func,
  downloadFile: PropTypes.func,
};

AppContent.defaultProps = {
  filesList: null,
  activeFileId: null,
  comments: null,
  newCommentValue: '',
  fileName: '',
  setActiveFileId: null,
  setNewComment: null,
  addNewComment: null,
  downloadFile: null,
};

export { AppContent };
