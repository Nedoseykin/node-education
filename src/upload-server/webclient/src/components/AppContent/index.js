import { connect } from 'react-redux';

import { AppContent } from "./AppContent";
import {
  activeFileIdSelector,
  commentsByFileIdSelector,
  filesListSelector,
  fileNameSelector,
  newCommentSelector,
} from '../../store/selectors';
import {
  acNetworkUpdateFileComments,
  acSetActiveFileId,
  acSetNewComment,
} from '../../store/actions';
import { commentsUrl, getFileUrl } from '../../network/urls';
import request from '../../network/request';

const setActiveFileId = activeFileId => dispatch => dispatch(acSetActiveFileId({ activeFileId }));

const setNewComment = e => dispatch => dispatch(acSetNewComment({ value: e.target.value }));

const addNewComment = (fileId, comment) => dispatch => {
  console.log('addNewComment..., fileId: ', fileId, ', comment: ', comment);
  request({
    url: commentsUrl,
    options: {
      method: 'POST',
    },
    body: JSON.stringify({ fileId, comment }),
  })
    .then((loadedData) => {
      // console.log('SUCCESS: ', loadedData);
      dispatch(acNetworkUpdateFileComments({ fileId, data: { loadStatus: 0 } }));
      dispatch(acSetNewComment({ value: '' }));
    })
    .catch((errObj) => { console.error('ERROR: ', errObj); });
};

const downloadFile = (fileId) => dispatch => {
  console.log('download file', fileId);
  request({ url: `${getFileUrl}/${fileId}` })
    .then((loadedData) => { console.log('SUCCESS: file has been downloaded: ', loadedData); })
    .catch((errObj) => { console.error('ERROR: failed to download: ', errObj); });
};

const mapStateToProps = state => ({
  filesList: filesListSelector(state),
  activeFileId: activeFileIdSelector(state),
  comments: commentsByFileIdSelector(state),
  fileName: fileNameSelector(state),
  newCommentValue: newCommentSelector(state),
});

const mapDispatchToProps = {
  setActiveFileId,
  setNewComment,
  addNewComment,
  downloadFile,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppContent);
