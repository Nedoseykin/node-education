import { connect } from 'react-redux';

import { AppHeader } from './AppHeader';
import { acMenuHide, acMenuShow } from '../../store/actions';

const showMenu = event => (dispatch) => {
  const { clientX: left, clientY: top } = event;
  dispatch(acMenuShow({ left, top }));
};

const closeMenu = () => (dispatch) => {
  dispatch(acMenuHide());
};

const mapStateToProps = state => ({
  isMenuVisible: state.ui.isMenuVisible,
  menuTop: state.ui.menuTop,
  menuLeft: state.ui.menuLeft,
});

const mapDispatchToProps = {
  showMenu,
  closeMenu,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppHeader);
