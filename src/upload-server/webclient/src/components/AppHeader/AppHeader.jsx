import React from 'react';
import * as PropTypes from 'prop-types';
import styles from './AppHeader.module.scss';

import MainMenu from '../menus/MainMenu/MainMenu';
import IconButton from '../buttons/IconButton/IconButton';
import { Menu as MenuIcon } from '../icons';
import { BUTTON_TYPES } from '../../constants';

const AppHeader = ({
  title,
  withMenuButton,
  isMenuVisible,
  menuTop,
  menuLeft,
  showMenu,
  closeMenu,
}) => (
  <header className={styles.AppHeader}>
    {
      withMenuButton && (
        <IconButton
          className={styles.menuButton}
          icon={MenuIcon}
          type={BUTTON_TYPES.contrast}
          onClick={showMenu}
        />
      )
    }
    <h1 className={styles.title}>{title}</h1>
    <MainMenu
      isVisible={isMenuVisible}
      top={menuTop}
      left={menuLeft}
      onClose={closeMenu}
    />
  </header>
);

AppHeader.propTypes = {
  title: PropTypes.string,
  withMenuButton: PropTypes.bool,
  isMenuVisible: PropTypes.bool,
  showMenu: PropTypes.func,
  closeMenu: PropTypes.func,
};

AppHeader.defaultProps = {
  withMenuButton: false,
  title: '',
  isMenuVisible: false,
  showMenu: () => {},
  closeMenu: () => {},
};

export { AppHeader };
