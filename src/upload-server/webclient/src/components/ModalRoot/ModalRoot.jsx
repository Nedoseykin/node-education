import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './ModalRoot.module.scss';
import {MODAL_ROOT_ID} from "../../constants";

const ModalRoot = () => (
    <div
      id={MODAL_ROOT_ID}
      className={styles.ModalRoot}
    />
  );

ModalRoot.propTypes = {
  isVisible: PropTypes.bool,
};

ModalRoot.defaultProps = {
  isVisible: false,
};

export default ModalRoot;
