import React from 'react';
import BasicButton from '../BasicButton/BasicButton';
import classNames from 'classnames';

import './IconButton.scss';

const IconButton = ({
  disabled,
  className,
  label,
  icon,
  type,
  onClick,
}) => (
  <BasicButton
    className={classNames('IconButton', className)}
    icon={icon}
    label={label}
    type={type}
    disabled={disabled}
    onClick={onClick}
  />
);

export default IconButton;
