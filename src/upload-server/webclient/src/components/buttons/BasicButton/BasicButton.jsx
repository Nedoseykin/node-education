import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import './BasicButton.scss';
import { isFilledString } from '../../../utils';
import { BUTTON_TYPES } from '../../../constants';

const BasicButton = ({
  className,
  icon: ButtonIcon,
  type,
  label,
  disabled,
  onClick,
}) => (
  <button
    className={classNames('BasicButton__root', type, disabled && 'Button_disabled', className)}
    onClick={!disabled ? onClick : null}
  >
    {
      ButtonIcon && (
        <span key="icon" className="BasicButton__icon">
          <ButtonIcon />
        </span>
      )
    }
    {
      isFilledString(label) && (
        <span key="label" className="BasicButton__label">
          {label}
        </span>
      )
    }
  </button>
);

BasicButton.propType = {
  icon: PropTypes.node,
  label: PropTypes.string,
  type: PropTypes.oneOf([
    BUTTON_TYPES.primary,
    BUTTON_TYPES.secondary,
    BUTTON_TYPES.contrast,
  ]),
  onClick: PropTypes.func,
};

BasicButton.defaultProps = {
  icon: null,
  label: '',
  type: BUTTON_TYPES.secondary,
  onClick: () => {},
};

export default BasicButton;
