import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Preloader.module.scss';

import ModalGlass from '../ModalGlass/ModalGlass';

const Preloader = ({
  isVisible,
}) => isVisible && (
  <ModalGlass>
    <div className={styles.Preloader__root}>
      <span className={styles.Preloader__message}>
        Fetch data is in progress...
      </span>
    </div>
  </ModalGlass>
);

Preloader.propTypes = {
  isVisible: PropTypes.bool,
};

Preloader.defaultProps = {
  isVisible: false,
};

export { Preloader };
