import { connect } from 'react-redux';

import { Preloader } from './Preloader';
import { preloaderIsVisibleSelector } from '../../store/selectors';

const mapStateToProps = state => ({
  isVisible: preloaderIsVisibleSelector(state),
});

export default connect(mapStateToProps)(Preloader );
