import React from 'react';
import { Provider } from 'react-redux';

import AppRoot from "../AppRoot/AppRoot";
import ModalRoot from '../ModalRoot/ModalRoot';
import DataService from '../DataService';
import Preloader from '../Preloader';

import configureStore from '../../store/configureStore';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <>
        <ModalRoot />
        <AppRoot />
        <DataService />
        <Preloader />
      </>
    </Provider>
  );
}

export default App;
