import { connect } from 'react-redux';

import { DataService } from './DataService';
import { acNetworkSetData, acNetworkUpdateFile } from '../../store/actions';

import {
  activeFileIdSelector,
  commentsByFileIdSelector,
  filesListLoadStatusSelector,
  filesListSelector
} from '../../store/selectors';
import { filesListURL, commentsUrl } from '../../network/urls';
import request from '../../network/request';

const setData = payload => (dispatch) => {
  dispatch(acNetworkSetData(payload));
};

const loadFilesList = () => (dispatch) => {
  // console.log('loadFilesList...');
  request({ url: filesListURL })
    .then((loadedData) => {
      dispatch(acNetworkSetData({
        data: { filesList: loadedData, filesListLoadStatus: 2 },
        requestsIncrement: -1,
      }));
    })
    .catch((errObj) => {
      console.error('ERROR', errObj);
      dispatch(acNetworkSetData({
        data: { filesList: null, filesListLoadStatus: 3 },
        requestsIncrement: -1,
      }));
    });
};

const loadCommentsByFileId = fileId => (dispatch) => {
  // console.log('loadCommentsByFileId... fileId:', fileId);
  request({ url: `${commentsUrl}/${fileId}` })
    .then((loadedData) => {
      // console.log('comments: loadedData: ', loadedData);
      dispatch(acNetworkUpdateFile({
        fileId,
        data: { comments: { loadStatus: 2, items: loadedData } },
        requestsIncrement: -1,
      }));
    })
    .catch((errObj) => {
      console.log('ERROR', errObj);
      dispatch(acNetworkUpdateFile({
        fileId,
        data: { comments: { loadStatus: 3, items: null } },
        requestsIncrement: -1,
      }))
    })
};

const mapStateToProps = state => ({
  filesList: filesListSelector(state),
  filesListLoadStatus: filesListLoadStatusSelector(state),
  comments: commentsByFileIdSelector(state),
  activeFileId: activeFileIdSelector(state),
});

const mapDispatchToProps = {
  setData,
  loadFilesList,
  loadCommentsByFileId,
};

export default connect(mapStateToProps, mapDispatchToProps)(DataService);
