import { PureComponent } from 'react';
import * as PropTypes from 'prop-types';
import { set, get } from 'lodash';
import { isExists, isFilledArray } from '../../utils';

class DataService extends PureComponent {
  static propTypes = {
    filesList: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        comments: PropTypes.shape({}),
      }),
    ),

    filesListLoadStatus: PropTypes.oneOf([0, 1, 2, 3]),

    comments: PropTypes.shape({
      loadStatus: PropTypes.oneOf([0, 1, 2, 3]),
      items: PropTypes.arrayOf(
        PropTypes.shape({
          id: PropTypes.number,
          comment: PropTypes.string,
        }),
      ),
    }),

    activeFileId: PropTypes.number,
    setData: PropTypes.func,
    loadFilesList: PropTypes.func,
    loadCommentsByFileId: PropTypes.func,
  };

  static defaultProps = {
    filesList: null,
    filesListLoadStatus: 2,
    comments: null,
    activeFileId: null,
    setData: () => {},
    loadFilesList: () => {},
    loadCommentsByFileId: () => {},
  };

  componentDidMount() {
    this.prepareData();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    this.prepareData(prevProps, prevState);
  }

  prepareData = (prevProps = {}, prevState = {}) => {
    const {
      comments,
      filesList,
      filesListLoadStatus,
      loadFilesList,
      loadCommentsByFileId,
      activeFileId,
      setData,
    } = this.props;

    let payload = { requestsIncrement: null, data: null };

    if ((filesListLoadStatus) === 2 && (filesList === null)) {
      set(payload, 'data.filesListLoadStatus', 0);
    }

    if (filesListLoadStatus === 0) {
      set(payload, 'data.filesListLoadStatus', 1);
      set(payload, 'requestsIncrement', 1);
      loadFilesList();
    }

    if (
      isFilledArray(filesList)
      && (filesListLoadStatus === 2)
      && isExists(activeFileId)
    ) {
      let loadStatus = get(comments, 'loadStatus', 0);
      console.log('comments: loadStatus: ', loadStatus);
      if (loadStatus === 0) {
        // noinspection JSObjectNullOrUndefined
        const newFilesList = filesList.map((item) => {
          if (item.id === activeFileId) {
            set(item, 'comments.loadStatus', 1);
          }
          return { ...item, comments: { ...item.comments } }
        });
        set(payload, 'data.filesList', newFilesList);
        set(payload, 'requestsIncrement', 1);
        loadCommentsByFileId(activeFileId);
      }
    }

    if ((payload.data !== null)
      || (payload.requestsIncrement !== null)) { setData(payload); }
  };

  render() {
    return null;
  }
}

export { DataService };
