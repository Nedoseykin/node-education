import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import ListItem from '../ListItem/ListItem';
import { isFilledArray } from '../../utils';
import styles from './List.module.scss';

const List = ({
  itemsList,
  getIsSelected,
  onItemClick,
}) => (
  <div className={styles.List__root}>
    {
      isFilledArray(itemsList)
        ? (
          <ul className={classNames('List__ul', styles.list)}>
            {
              itemsList.map((item) => {
                const { id, name } = item;
                return (
                  <ListItem
                    key={id}
                    isSelected={getIsSelected(id)}
                    onClick={onItemClick ? () => onItemClick(id) : null}
                  >
                    {name}
                  </ListItem>
                )
              })
            }
          </ul>
        )
        : 'No data is available'
    }
  </div>
);

List.propTypes = {
  itemsList: PropTypes.arrayOf(PropTypes.shape({})),
  onItemClick: PropTypes.func,
  getIsSelected: PropTypes.func,
};

List.defaultProps = {
  itemsList: null,
  onItemClick: null,
  getIsSelected: () => false,
};

export default List;
