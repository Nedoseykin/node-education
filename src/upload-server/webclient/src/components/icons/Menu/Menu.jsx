import React from "react";

const Menu = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg" version="1.1"
    viewBox="0 0 32 29"
    preserveAspectRatio="xMidYMid meet"
    width="100%"
    height="100%"
  >
    <path
      d="M1.5 2.5 H29 M1 10.5 H29 M1 18.5 H29 M1 26.5 H29"
      stroke="rgba(0, 0, 0, 1)"
      strokeWidth="2.5"
      strokeLinecap="round"
      fill="none"
    />
  </svg>
);

export default Menu;
