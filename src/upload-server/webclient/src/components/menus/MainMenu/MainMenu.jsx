import React, { PureComponent } from 'react';
import * as PropTypes from 'prop-types';

import ModalGlass from '../../ModalGlass/ModalGlass';

import styles from './MainMenu.module.scss';

class MainMenu extends PureComponent {
  static propTypes = {
    top: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    left: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    isVisible: PropTypes.bool,
    onClose: PropTypes.func,
  };

  static defaultProps = {
    top: '50vh',
    left: '50vw',
    isVisible: false,
    onClose: () => {},
  };

  componentDidMount() {
    this.setStyles();
  }

  componentDidUpdate() {
    this.setStyles();
  }

  setStyles = () => {
    if (this.menu) {
      const {
        offsetHeight: height,
        offsetWidth: width,
        offsetLeft: left,
        offsetTop: top,
      } = this.menu;

      this.menu.style.top = `${top + height / 2}px`;
      this.menu.style.left = `${left + width / 2}px`;
    }
  };

  render() {
    const { top, left, isVisible, onClose } = this.props;

    return isVisible && (
      <ModalGlass
        onClose={onClose}
      >
        <div
          className={styles.MainMenu}
          style={{ left: left || '50vw', top: top || '50vh' }}
          ref={(elm) => { this.menu = elm; }}
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
          }}
        >
          Main menu
        </div>
      </ModalGlass>
    );
  }
}

export default MainMenu;
