import React from 'react';

import AppHeader from "../AppHeader";
import AppContent from "../AppContent";
import {APP_ROOT_ID} from "../../constants";

const AppRoot = () => (
  <div id={APP_ROOT_ID}>
    <AppHeader title="Files Uploader" withMenuButton={true} />
    <AppContent />
  </div>
);

export default AppRoot;
