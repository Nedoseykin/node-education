import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './ListItem.module.scss';

const CLASSES = {
  BUTTON: 'ListItem_button',
  DISABLED: 'ListItem_disabled',
  SELECTED: 'ListItem_selected',
};

const ListItem = ({
  children,
  isSelected,
  disabled,
  onClick,
}) => {
  return (
    <li
      className={classNames(
        styles.ListItem__root,
        onClick && styles[CLASSES.BUTTON],
        isSelected && styles[CLASSES.SELECTED],
        disabled && styles[CLASSES.DISABLED],
        'ListItem__root',
        onClick && 'ListItem__button',
        isSelected && 'ListItem__selected',
        disabled && 'ListItem__disabled',
      )}
      onClick={onClick}
    >
      {children}
    </li>
  );
};

ListItem.propTypes = {
  children: PropTypes.any,
  isSelected: PropTypes.bool,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
};

ListItem.defaultProps = {
  children: [],
  isSelected: false,
  disabled: false,
  onClick: null,
};

export default ListItem;
