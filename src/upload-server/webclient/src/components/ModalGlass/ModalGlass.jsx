import React, { PureComponent } from 'react';
import ReactDom from 'react-dom';
import * as PropTypes from 'prop-types';

import { MODAL_ROOT_ID } from "../../constants";
import styles from './ModalGlass.module.scss';

class ModalGlass extends PureComponent {
  static propTypes = {
    onClose: PropTypes.func,
  };

  static defaultProps = {
    onClose: () => {},
  };

  constructor(props) {
    super(props);
    this.elm = document.createElement('div');
    this.elm.setAttribute('class', 'modal_child');
  }

  componentDidMount() {
    const modalRoot = document.getElementById(MODAL_ROOT_ID);
    modalRoot.appendChild(this.elm);
  }

  componentWillUnmount() {
    const modalRoot = document.getElementById(MODAL_ROOT_ID);
    modalRoot.removeChild(this.elm);
  }

  render() {
    const { onClose, children } = this.props;

    return ReactDom.createPortal(
      <div
        className={styles.ModalGlass}
        onClick={onClose}
      >
        {children}
      </div>,
      this.elm
    );
  }
}

export default ModalGlass;
