import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import List from '../List/List';

import './CentralSection.scss';
import styles from './CentralSection.module.scss';
import { isExists, isFilledArray, isFilledString } from '../../utils';

const CentralSection = ({
  id,
  title,
  comments,
  onNewCommentChange,
  newCommentValue,
  addNewComment,
  downloadFile,
}) => {
  const isError = comments && (comments.loadStatus === 3);
  const commentsList = comments && isFilledArray(comments.items)
    ? comments.items.map(({ id, comment }) => ({ id, name: comment }))
    : [];
  const isNewCommentValue = isFilledString(newCommentValue) && isFilledString(newCommentValue.trim());
  const isFileSelected = isExists(id);

  return(
    <section className={styles.CentralSection__root}>
      <div key="title" className={styles.CentralSection__title}>
        <div key="header" className={styles.title__header}>
          {title}
        </div>
        <div key="toolbar" className={styles.title__toolbar}>
          {isFileSelected && (
            <span
              className={styles.title_toolbar__btn}
              onClick={() => { downloadFile(id); }}
            >
              Download
            </span>
          )}
        </div>
      </div>
      <div key="content" className={classNames('CentralSectionContent_list', styles.CentralSection__content)}>
        {
          isError && (
            <div key="error">
              Error
            </div>
          )
        }
        {
          !isError && (
            <div key="comments" className={styles.content__comments}>
              <div key="list" className={styles.comments__list}>
                <List
                  itemsList={commentsList}
                />
              </div>
              {
                isExists(id) && (
                  <div key="newComment" className={styles.comment__new}>
                    <div key="newCommentContent" className={styles.comment__new__content}>
                      <textarea
                        name="newComment"
                        id="newComment"
                        cols="30"
                        rows="10"
                        value={newCommentValue}
                        onChange={onNewCommentChange}
                      />
                    </div>
                    <div key="newCommentToolbar" className={styles.comment__new__toolbar}>
                      <span
                        className={classNames(styles.toolbar__btn, !isNewCommentValue && styles.toolbar__btn_disabled)}
                        onClick={() => {
                          addNewComment && isNewCommentValue && addNewComment(id, newCommentValue);
                        }}
                      >
                        Add
                      </span>
                    </div>
                  </div>
                )
              }
            </div>
          )
        }
      </div>
    </section>
  )
};

CentralSection.propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  title: PropTypes.string,
  comments: PropTypes.shape({}),
  onNewCommentChange: PropTypes.func,
  newCommentValue: PropTypes.string,
  addNewComment: PropTypes.func,
  downloadFile: PropTypes.func,
};

CentralSection.defaultProps = {
  id: null,
  title: '',
  comments: null,
  onNewCommentChange: null,
  newCommentValue: '',
  addNewComment: null,
  downloadFile: null,
};

export default CentralSection;
