// true - mock data
import filesList from './files-list';

export default {
  filesList: {
    mock: true,
    value: filesList,
  },
};
