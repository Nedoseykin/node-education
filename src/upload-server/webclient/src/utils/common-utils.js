function isExists(value) {
  return (value !== undefined) && (value !== null);
}

function isFilledString(value) {
  return (typeof value === 'string') && (value.length > 0);
}

function isFilledArray(value) {
  return Array.isArray(value) && value.length > 0;
}

export {
  isExists,
  isFilledString,
  isFilledArray,
};
