import { get } from 'lodash';
import { isFilledString } from './common-utils';

export function getModel(rootModel, model) {
  return [rootModel, model].filter(item => isFilledString(item)).join('.');
}

export function getBasicSelectorCreator(rootModel) {
  return (model, defaultValue) => state => get(state, getModel(rootModel, model), defaultValue);
}
