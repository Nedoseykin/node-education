export default async ({
  url,
  options = {},
  body = null,
}) => {
  // console.log('options: ', options);
  const response = await fetch(url, { ...options, body });

  const { status, ok, headers } = response;
  const contentType = headers.get('content-type');
  // console.log('content-type: ', contentType);
  let result = null;
  if (contentType && contentType.includes('json')) {
    result = await response.json();
  } else if (contentType && contentType.includes('text')) {
    result = await response.text();
  } else {
    result = response;
  }

  if (ok) {
    if (!result) { result = true; }
    return Promise.resolve(result);
  } else {
    let message = 'Network error';
    if (!result && (status >= 500)) {
      message = 'Internal server error';
    } else if (result && result.message) {
      ({ message } = result);
    }
    return Promise.reject({ message, status, date: new Date().getTime() });
  }
};
