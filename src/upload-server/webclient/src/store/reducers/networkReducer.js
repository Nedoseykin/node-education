import { handleActions } from 'redux-actions';
import { get } from 'lodash';

import {
  acNetworkSetData,
  acNetworkUpdateFile,
  acNetworkUpdateFileComments,
  acResetApp,
} from '../actions';
import { isExists, isFilledArray } from '../../utils';

const initialState = {
  requests: 0,
  filesListLoadStatus: 2,
  filesList: null,
};

export default handleActions({
  [acNetworkSetData]: (state, { payload }) => ({
    ...state,
    ...payload.data,
    requests: isExists(payload.requestsIncrement)
      ? state.requests + payload.requestsIncrement
      : state.requests,
  }),

  [acNetworkUpdateFile]: (state, { payload }) => {
    const { fileId = null, data = {}, requestsIncrement = 0 } = payload;
    let { filesList } = state;
    if (isFilledArray(filesList)) {
      filesList = filesList.map((item) => {
        return item.id === fileId ? { ...item, ...data } : { ...item };
      });
    }

    return {
      ...state,
      filesList,
      requests: state.requests + requestsIncrement,
    };
  },

  [acNetworkUpdateFileComments]: (state, { payload }) => {
    const { fileId = null, data = {}, requestsIncrement = 0 } = payload;
    let { filesList } = state;
    filesList = filesList.map((item) => {
      if (item.id === fileId) {
        let comments = get(item, 'comments', {});
        item.comments = { ...comments, ...data };
      }
      return { ...item };
    });
    return {
      ...state,
      filesList,
      requests: state.requests + requestsIncrement,
    };
  },

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
