import { handleActions } from 'redux-actions';

import {
  acMenuShow,
  acMenuHide,
  acMenuToggle,
  acResetApp,
  acSetActiveFileId,
  acSetNewComment,
} from '../actions';

const initialState = {
  isMenuVisible: false,
  menuLeft: '50vw',
  menuTop: '50vh',

  activeFileId: null,
  newComment: '',
};

export default handleActions({
  [acMenuShow]: (state, { payload }) => ({
    ...state,
    isMenuVisible: true,
    menuTop: payload.top,
    menuLeft: payload.left,
  }),

  [acMenuHide]: (state) => ({
    ...state,
    isMenuVisible: false,
  }),

  [acMenuToggle]: (state) => ({
    ...state,
    isMenuVisible: !state.isMenuVisible,
  }),

  [acSetActiveFileId]: (state, { payload }) => ({
    ...state,
    activeFileId: payload.activeFileId,
    newComment: '',
  }),

  [acSetNewComment]: (state, { payload }) => ({
    ...state,
    newComment: payload.value,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
