import { combineReducers } from "redux";

import ui from './uiReducer';

import network from './networkReducer';

export default combineReducers({
  ui,
  network,
});
