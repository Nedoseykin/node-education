import { createAction } from 'redux-actions';

import {
  AC_MENU_SHOW,
  AC_MENU_HIDE,
  AC_MENU_TOGGLE,
  AC_SET_ACTIVE_FILE_ID,
  AC_SET_NEW_COMMENT,
} from './actionTypes';

export const acMenuShow = createAction(AC_MENU_SHOW);

export const acMenuHide = createAction(AC_MENU_HIDE);

export const acMenuToggle = createAction(AC_MENU_TOGGLE);

export const acSetActiveFileId = createAction(AC_SET_ACTIVE_FILE_ID);

export const acSetNewComment = createAction(AC_SET_NEW_COMMENT);
