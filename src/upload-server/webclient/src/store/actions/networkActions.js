import { createAction } from 'redux-actions';

import {
  AC_NETWORK_SET_DATA,
  AC_NETWORK_UPDATE_FILE,
  AC_NETWORK_UPDATE_FILE_COMMENTS,
} from './actionTypes';

export const acNetworkSetData = createAction(AC_NETWORK_SET_DATA);

export const acNetworkUpdateFile = createAction(AC_NETWORK_UPDATE_FILE);

export const acNetworkUpdateFileComments = createAction(AC_NETWORK_UPDATE_FILE_COMMENTS);
