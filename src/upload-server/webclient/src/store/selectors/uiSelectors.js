import { getBasicSelectorCreator } from '../../utils';

const createBasicSelector = getBasicSelectorCreator('ui');

const activeFileIdSelector = createBasicSelector('activeFileId');

const newCommentSelector = createBasicSelector('newComment');

export {
  activeFileIdSelector,
  newCommentSelector,
};
