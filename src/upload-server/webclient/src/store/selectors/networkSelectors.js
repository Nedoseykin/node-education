import { createSelector } from 'reselect';
import { activeFileIdSelector } from './uiSelectors';
import { isExists, isFilledArray, getBasicSelectorCreator, isFilledString } from '../../utils';

const createBasicSelector = getBasicSelectorCreator('network');

const filesListSelector = createBasicSelector('filesList', null);

const filesListLoadStatusSelector = createBasicSelector('filesListLoadStatus', 2);

const networkRequestsSelector = createBasicSelector('requests');

const preloaderIsVisibleSelector = createSelector(
  [networkRequestsSelector],
  (requests) => {
    return requests > 0;
  },
);

const commentsByFileIdSelector = createSelector(
  [activeFileIdSelector, filesListSelector],
  (activeFileId, filesList) => {
    let comments = null;
    if (isExists(activeFileId) && isFilledArray(filesList)) {
      const file = filesList.find(item => item.id === activeFileId);
      if (file) { ({ comments } = file) }
    }
    return comments;
  },
);

const fileNameSelector = createSelector(
  [filesListSelector, activeFileIdSelector],
  (filesList, activeFileId) => {
    let fileName = '';
    if (isExists(activeFileId) && isFilledArray(filesList)) {
      const file = filesList.find(item => item.id === activeFileId);
      if (file && isFilledString(file.name)) { fileName = file.name; }
    }
    return fileName;
  },
);

export {
  preloaderIsVisibleSelector,
  filesListSelector,
  filesListLoadStatusSelector,
  commentsByFileIdSelector,
  fileNameSelector,
};
