const BUTTON_TYPES = {
  primary: 'Button__primary',
  secondary: 'Button__secondary',
  contrast: 'Button__contrast',
};

export default BUTTON_TYPES;
