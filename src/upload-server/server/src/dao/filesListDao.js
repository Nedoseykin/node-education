const fs = require('fs');
const createErrorObject = require('../Error/createError');
const { isFilledArray } = require('../utils/common-utils');

const {
  getFilesList,
} = require('../../configs/mock');

const readFilesList = async () => {
  const { mock, fileName } = getFilesList;
  if (mock === true) {
    console.log('DAO - readFilesList - mock data...');
    return  new Promise((resolve, reject) => {
      fs.readFile(fileName, 'utf-8', (err, data) => {
        if (!err) {
          try {
            const value = JSON.parse(data);
            return resolve(isFilledArray(value) ? value : []);
          } catch (e) {
            console.log('Error: ', e);
            const errObj = createErrorObject();
            reject(errObj);
          }
        } else {
          console.log('Error: ', err);
          let errObj = createErrorObject(403, 'Can not get of files list');

          if (!fs.existsSync(fileName)) {
            errObj = { ...errObj, message: 'Error: file is not found', status: 404 };
          }
          return reject(errObj);
        }
      });
    });
  }
};

module.exports = {
  readFilesList,
};
