const fs = require('fs');
const { isFilledArray } = require('../utils/common-utils');

const {
  getComments,
} = require('../../configs/mock');

const readComments = async () => {
  const { mock, fileName } = getComments;
  if (mock === true) {
    console.log('DAO - readComments - mock data...');
    return new Promise((resolve, reject) => {
      fs.readFile(fileName, 'utf-8', (err, data) => {
        if (!err) {
          try {
            const parsedData = JSON.parse(data);
            return resolve(isFilledArray(parsedData) ? parsedData : []);
          } catch (e) {
            console.log('Error: ', e);
            return reject(createErrorObject());
          }
        } else {
          console.log('Error:', err);
          let errObj = createErrorObject();
          if (!fs.existsSync(fileName)) {
            errObj = { ...errObj, message: 'Error: file is not found', status: 404 };
          }
          return reject(errObj);
        }
      });
    });
  }
};

const readCommentsByFileId = async (fileId) => {
  const comments = await readComments().catch((err) => {
    console.log('Error: readCommentsById: readComments():', err);
    return Promise.reject(err);
  });
  return new Promise((resolve, reject) => {
    try {
      const filteredComments = comments.filter(item => item.fileId === fileId);
      console.log('DAO - readCommentsByFileId - Success');
      resolve(filteredComments);
    } catch (e) {
      console.log('Error: ', e);
      reject(createErrorObject());
    }
  });
};

const writeComments = async (comments) => {
  let data = [];
  try {
    data = JSON.stringify(comments);
  } catch (e) {
    return Promise.reject({ status: 500, message: 'Error: wrong comments data for writing' });
  }
  const { mock, fileName } = getComments;
  if (mock === true) {
    console.log('DAO - writeComments - mock data...');
    return new Promise((resolve, reject) => {
      fs.writeFile(fileName, data, 'utf-8', (err) => {
        if (!err) {
          resolve(true);
        } else {
          reject({ status: 500, message: 'Error: comments writing failed' });
        }
      });
    });
  }
};

module.exports = {
  readComments,
  readCommentsByFileId,
  writeComments,
};
