function isFilledArray(v) {
  return Array.isArray(v) && v.length > 0;
}

function isFilledString(v) {
  return (typeof v === 'string') && v.length > 0;
}

module.exports = {
  isFilledArray,
  isFilledString,
};
