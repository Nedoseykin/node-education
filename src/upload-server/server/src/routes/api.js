const router = require('express').Router();
const { getFilesList, getFileById } = require('../services/filesListService');
const { getCommentsByFileId, addComment } = require('../services/commentsService');

router.get('/files-list', getFilesList);

router.get('/get-file/:id', (req, res) => {
  getFileById(req, res).catch(() => {});
});

router.post('/comments', (req, res) => {
  addComment(req, res).catch(() => {});
});

router.get('/comments/:id', (req, res) => {
  getCommentsByFileId(req, res).catch(() => {});
});

module.exports = router;
