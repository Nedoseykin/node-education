const path = require('path');
const fs = require('fs');
const { sha256 } = require('js-sha256');
const { readFilesList } = require('../dao/filesListDao');
const { isFilledArray, isFilledString } = require('../utils/common-utils');
const ServiceError = require('../Error/Error');
const createErrorObject = require('../Error/createError');

const getFilesList = async (req, res) => {
  console.log('Service: getFilesList...');
  try {
    let value = await readFilesList();
    // const valueStr = JSON.stringify(value);
    const eTag = sha256(value);
    const { 'if-none-match': ifNoneMatch } = req.headers;
    console.log('Success - send files list');
    // console.log('etag', eTag);
    // console.log('if-none-match', ifNoneMatch);
    if (eTag === ifNoneMatch) {
      // console.log('sendStatus...');
      res.sendStatus(304);
    } else {
      res
        .status(200)
        .append('content-type', 'application/json; charset=utf-8')
        .append('cache-control', 'public, max-age=0')
        .append('ETag', eTag)
        .send(value);
    }
  } catch (e) {
    const { message, status, ts } = e;
    console.log(`Error - ${new Date(ts).toLocaleString()} - ${status} - ${message}`);
    res.status(status || 500).send({ message, ts });
  }
};

const getFileById = async (req, res) => {
  const { id } = req.params;
  const fileId = parseInt(id, 10);
  console.log('Service: getFileById, id: ', fileId);
  let files = [];
  let status = 200;
  let data;
  try {
    files = await readFilesList();
    if (isFilledArray(files)) {
      const file = files.find(item => item.id === fileId);
      const name = file && isFilledString(file.name) ? file.name : '';
      const fileName = path.resolve(__dirname, '..', '..', 'mock-data', 'upload', name);
      const isExists = fs.existsSync(fileName);
      console.log('isExists: ', isExists);
      if (!isExists) {
        throw new ServiceError({ message: 'File does not exist', status: 404 });
      }
      const rs = fs.createReadStream(fileName);
      await new Promise((resolve, reject) => {
        res.append('content-disposition', 'attachment');
        rs
          .on('data', (data) => {
            console.log('data: ', data);
          })
          .on('close', () => {
            console.log('File transfer is finished');
          })
          .pipe(res);
      });

    }
  } catch (e) {
    status = e.status || 500;
    data = { message: e.message };
    console.log('Error: ', message);
    // res.status(status || 500).send({ message })
  } finally {
    if (!data) {
      res.sendStatus(status);
    } else {
      if (status >= 400) {
        console.log('Error: ', data);
        data = createErrorObject(status, data.message);
      }
      res.status(status).send(data);
    }
  }
};

module.exports = {
  getFilesList,
  getFileById,
};
