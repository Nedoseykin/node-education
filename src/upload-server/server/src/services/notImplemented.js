const notImplemented = (req, res) => {
  console.log('response - 501 - NOT IMPLEMENTED');
  res.status(501).json({ message: 'Not implemented' });
};

module.exports = {
  notImplemented,
};
