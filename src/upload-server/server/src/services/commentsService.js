const { sha256 } = require('js-sha256');
const createErrorObject = require('../Error/createError');
const { readComments, readCommentsByFileId, writeComments } = require('../dao/commentsDao');

const getCommentsByFileId = async (req, res) => {
  const id = parseInt(req.params.id, 10);
  console.log('Service: getComments... fileId:', id);
  try {
    const value = await readCommentsByFileId(id);
    const eTag = sha256(value);
    const { 'if-none-match': ifNoneMatch } = req.headers;
    if (eTag === ifNoneMatch) {
      res.sendStatus(304);
    } else {
      res
        .status(200)
        .append('content-type', 'application/json; charset=utf-8')
        .append('cache-control', 'public, max-age=0')
        .append('ETag', eTag)
        .send(value);
    }
  } catch (e) {
    let errObj = e && e.status ? e : createErrorObject();
    console.log('Error: ', e);
    res.status(errObj.status).send(errObj);
  }
};

const addComment = async (req, res) => {
  const { body } = req;
  let data = {};
  try {
    data = JSON.parse(body);
    const { fileId, comment } = data;
    console.log(`POST - /comments - { fileId: ${fileId}, comment: ${comment} }`);
    let comments = await readComments();

    if (!Array.isArray(comments)) { comments = []; }
    let id = 0;
    comments.forEach((item) => { id = Math.max(item.id, id) });
    id++;
    const newComment = { id, fileId, comment };
    comments.push(newComment);
    const result = await writeComments(comments);
    if (result) {
      res.status(200).json(newComment);
    } else {
      console.log('Error: writing of comment is failed');
      res.status(500).json(createErrorObject());
    }
  } catch (e) {
    const errObj = createErrorObject(400);
    res.status(400).json(errObj);
  }
};

module.exports = {
  getCommentsByFileId,
  addComment,
};
