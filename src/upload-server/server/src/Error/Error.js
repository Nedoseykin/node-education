function ServiceError({ message, status }) {
  this.message = message;
  this.status = status;
  return this;
}

module.exports = ServiceError;
