const { isFilledString } = require('../utils/common-utils');

const createErrorObject = (errStatus, errMessage) => {
  const status = errStatus || 500;
  let message = errMessage;
  if (status >= 500) {
    message = 'Internal server error. Please, contact with service administrator.';
  } else if (status === 401) {
    message = 'Request is not authorized.';
  } else if (status === 404) {
    message = `Resource was not found.${isFilledString(errMessage) ? ` ${errMessage}` : ''}`;
  } else if ((status >= 400) && (status <= 500)) {
    message = `Request error.${isFilledString(errMessage) ? ` ${errMessage}` : ''}`;
  }
  return { status, message: `Error: ${message}`, ts: new Date().getTime() };
};

module.exports = createErrorObject;
