const path = require('path');

const mockConfig = {
  getFilesList: {
    mock: true,
    fileName: path.resolve(__dirname, '..', 'mock-data', 'files-list.json'),
  },
  getComments: {
    mock: true,
    fileName: path.resolve(__dirname, '..', 'mock-data', 'comments.json'),
  },
};

module.exports = mockConfig;
