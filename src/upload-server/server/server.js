const path = 'path';

const express = require('express');

const bodyParser = require('body-parser');

const helmet = require('helmet');

const apiRoute = require('./src/routes/api');

const { notImplemented } = require('./src/services/notImplemented');

const port = 8000;

const server = express();

server.set('etag', 'strong');

server.use(helmet());

server.use(bodyParser.urlencoded({ extended: true }));

server.use(bodyParser.json());

server.use(bodyParser.text());

server.use('/', express.static(path.resolve(__dirname, 'public')));

server.use((req, res, next) => {
    console.log('request: ', req.originalUrl);
    next();
});

server.use('/api', apiRoute);

server.all('*', notImplemented);

server.listen(port, () => {
    console.log(`Server listens on port ${port}`);
});
