import { get } from 'lodash';
import { createSelector } from 'reselect';

const requestUrlBasicSelector = state => get(state, 'request.requestUrl', '');

const requestUrlSelector = createSelector(
  [requestUrlBasicSelector],
  (requestUrl) => {
    // todo validation and url changes
    return requestUrl;
  },
);

export {
  requestUrlSelector,
};
