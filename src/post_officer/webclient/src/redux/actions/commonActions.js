import { createAction } from 'redux-actions';

export const acResetApp = createAction('AC_RESET_APP');
