import { createAction } from 'redux-actions';

export const acSetRequestUrl = createAction('AC_SET_REQUEST_URL');
