import { handleActions } from 'redux-actions';

import { basicAction, acResetApp } from '../actions';

const initialState = {
  temp: 0,
};

export default handleActions({
  [basicAction]: (state, { payload }) => ({
    ...state,
    temp: payload.value,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
