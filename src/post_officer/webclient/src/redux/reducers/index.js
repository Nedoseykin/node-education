import { combineReducers } from 'redux';

import basic from './basicReducer';
import request from './requestReducer';

export default combineReducers({
  basic,
  request,
});
