import { handleActions } from 'redux-actions';

import { acSetRequestUrl, acResetApp } from '../actions';

const initialState = {
  requestUrl: '',
};

export default handleActions({
  [acSetRequestUrl]: (state, { payload }) => ({
    ...state,
    requestUrl: payload.requestUrl,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
