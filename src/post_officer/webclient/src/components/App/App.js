import React from 'react';
import { Provider } from 'react-redux';
import './App.css';

import Content from '../Content';

import configStore from '../../redux/configStore';

const store = configStore();

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header>
          Post Officer
        </header>
        <Content />
      </div>
    </Provider>
  );
}

export default App;
