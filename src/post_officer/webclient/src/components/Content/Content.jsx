import React from 'react';
import * as PropTypes from 'prop-types';

async function request (url) {
  try {
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    });
    if (response.ok) {
      return await response.json();
    }
  } catch (e) {
    return { message: e };
  }
}

const Content = ({
  requestUrl,
  handleChangeUrl,
}) => {
  const handleSendRequest = () => {
    console.log('send: ', requestUrl);
    request(requestUrl)
      .then(data => console.log('data: ', data))
      .catch(err => console.log('error: ', err));
  };

  return (
    <section className="Content__root">
      <div key="requestUrl" className="Content__rows">
        <label htmlFor="requestUrl">URL</label>
        <input id="requestUrl" type="text" value={requestUrl} onChange={handleChangeUrl} />
      </div>
      <div key="buttonSend" className="Content__rows">
        <button
          className="Button Button__send"
          onClick={handleSendRequest}
        >
          Send
        </button>
      </div>
    </section>
  )
};

Content.propTypes = {
  requestUrl: PropTypes.string,
  handleChangeUrl: PropTypes.func.isRequired,
};

Content.defaultProps = {
  requestUrl: '',
};

export { Content };
