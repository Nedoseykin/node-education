import { connect } from 'react-redux';

import { Content } from './Content';
import { acSetRequestUrl } from '../../redux/actions';
import { requestUrlSelector } from '../../redux/selectors/requestSelectors';

const handleChangeUrl = e => (dispatch) => {
  const { value: requestUrl } = e.target;
  dispatch(acSetRequestUrl({ requestUrl }));
};

const mapStateToProps = state => ({
  requestUrl: requestUrlSelector(state),
});

const mapDispatchToProps = {
  handleChangeUrl,
};

export default connect(mapStateToProps, mapDispatchToProps)(Content);
