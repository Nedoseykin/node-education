const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const { log, requestLogger } = require('./src/logger/log');

const { request } = require('./src/routes/index');
const { errorPageService } = require('./src/services/index');

const savePid = require('./src/helper/pid').savePid;

const port = 8010;

const { pid } = process;
savePid(pid);

const server = express();

server.use(requestLogger);

server.use(cors());

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

server.use('/assets', express.static('./public'));

server.use('/request', request);

server.all('*', errorPageService);

server.listen(port, () => {
  log(`Server listens on port ${port}`);
});
