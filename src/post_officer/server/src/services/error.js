const { log } = require('../logger/log');

function errorPageService(req, res) {
  log(`404 - Error: not found - ${req.originalUrl}`);
  res
    .status(404)
    .json({
      message: 'Not found',
    });
}

module.exports = errorPageService;
