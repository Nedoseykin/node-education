const fs = require('fs');
const path = require('path');
const os = require('os');

const logFileDirectory = path.resolve(__dirname, '..', '..', 'logs');
const logFileName = path.resolve(logFileDirectory, 'server.log');
console.log('logFileName: ', logFileName);

async function log(message) {
  const date = new Date().toUTCString();
  const logString = `${date} - ${message}`;

  console.log(logString);

  try {
    const isDirectoryExists = fs.existsSync(logFileDirectory);
    if (!isDirectoryExists) {
      await fs.mkdir(logFileDirectory, (err) => {
        if (err) { throw new Error(err.message)}
      });
    }
    fs.appendFile(logFileName, `${logString}${os.EOL}`, 'utf8', (err) => {
      if (err) {
        throw new Error(err.message);
      }
    });
  } catch (e) {
    console.error(`${date} - Error to save log - `, e);
  }
}

function requestLogger(req, res, next) {
  const { method, originalUrl } = req;
  log(`${method} - ${originalUrl}`);
  next();
}

module.exports = {
  log,
  requestLogger,
};
