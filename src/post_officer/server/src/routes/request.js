const { Router } = require('express');
const { log } = require('../logger/log');

const router = new Router();

router.route('/run')
  .get((req, res) => {
    const { query, params, body } = req;
    log(`200 - run request - GET - ${req.originalUrl}`);
    res.status(200).send({ params, query, body });
  })
  .post((req, res) => {
    log(`501 - Error: not implemented - POST: ${req.originalUrl}`);
    res.status(501).send('not implemented');
  });

module.exports = router;
