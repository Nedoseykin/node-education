const fs = require('fs');
const path = require('path');

const { log } = require('../logger/log');

const pidFileName = path.resolve(__dirname, '..', '..', 'server.pid');

async function savePid(pid) {
  try {
    fs.writeFile(pidFileName, `pid: ${pid}`, 'utf-8', (err) => {
      if (err) throw new Error(err);
      log(`Pid has been saved: ${pid}`)
    });
  } catch (e) {
    log(e);
  }
}

module.exports = {
  savePid,
};
