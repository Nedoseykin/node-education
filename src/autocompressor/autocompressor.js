const readline = require('readline');
const path = require('path');
const fs = require('fs');
const zlib = require('zlib');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on('close', () => {
  rl.write('Auto-compressor is going to close\n\n');
  process.exit(0);
});

rl.write(`Auto-compressor is running in ${process.env.NODE_ENV || 'development'} mode\n`);

getPathToDirectory();

//functions
function getPathToDirectory() {
  rl.question('\n> Enter the path to directory: ', getAnswer);
}

function getAnswer(answer) {
  if (!answer) rl.close();

  const dirFullName = getDirFullName(answer);
  rl.write(`\nTarget path: ${dirFullName}\n`);

  if (processDirectory(dirFullName) === false) {
    getPathToDirectory();
  }
}

function getDirFullName(dirName) {
  if (dirName.indexOf('.') === 0) {
    return path.resolve(__dirname, dirName);
  }
  return dirName;
}

function processDirectory(dirFullName) {
  if (!fs.existsSync(dirFullName)) {
    rl.write('Directory does not exists\n\n');
    return false;
  }

  const dirStat = fs.statSync(dirFullName);
  if (!dirStat.isDirectory()) {
    rl.write('Object is not a directory\n\n');
    return false;
  }

  rl.write('Directory was found. Start analysis...\n');

  let files = analyseDir(dirFullName);

  rl.write(`${files.length} file${files.length === 1 ? ' was' : 's were'} found for processing.\n`);

  rl.write('Start processing...\n');

  processFiles(files);

  return true;
}

function analyseDir(dirFullName, filesArray = []) {
  let filesInDir = [];
  const items = fs.readdirSync(dirFullName);
  items.forEach((name) => {
    const fullName = path.resolve(dirFullName, name);
    const itemStat = fs.statSync(fullName);
    if (itemStat.isDirectory()) {
      filesInDir = analyseDir(fullName, filesInDir);
    } else if (path.extname(name) !== '.gz') {
      filesInDir.push({
        name,
        fullName,
        mTime: itemStat.mtimeMs,
      });
    }
  });
  return Array.isArray(filesArray) ? filesArray.concat(filesInDir) : filesInDir;
}

function processFiles(files) {
  if (Array.isArray(files) && files.length > 0) {
    processFileByIndex(files, 0);
  }
}

function processFileByIndex(files, index) {
  if (index < files.length) {
    files[index].hasBeenProcessed = true;
    const { fullName, mTime } = files[index];
    rl.write(`${index + 1} ==> "${fullName}" ==> `);
    const fullArchName = `${fullName}.gz`;

    const isArchExists = fs.existsSync(fullArchName);

    rl.write(`archive was${isArchExists ? ' ' : ' not '}found`);

    if (isArchExists) {
      const archStat = fs.statSync(fullArchName);

      let isExpired = archStat.mtimeMs <= mTime;

      if (!isExpired) {
        rl.write(', status: actual\n');
        return processFileByIndex(files, index + 1);
      }

      rl.write(', status: expired');
    }

    rl.write(' - Compression is started');

    const inp = fs.createReadStream(fullName);
    const out = fs.createWriteStream(fullArchName);
    const gzip = zlib.createGzip();

    inp
      .pipe(gzip)
      .pipe(out)
      .on('close', () => {
        files[index].hasBeenCompressed = true;
        rl.write(` ...success (file ${!isArchExists ? 'has been created' : 'has been re-wrote'})\n`);
        processFileByIndex(files, index + 1);
      });
  } else {
    const totalHaveBeenCompressed = files.filter(file => file.hasBeenCompressed).length;
    const totalHaveBeenProcessed = files.filter(file => file.hasBeenProcessed).length;

    rl.write(`\nProcessing has been completed: ${
      getCountWasStr(totalHaveBeenProcessed)
    } processed, ${
      getCountWasStr(totalHaveBeenCompressed)
    } compressed\n\n`);

    getPathToDirectory();
  }
}

function getCountWasStr(count) {
  return `${count} file${count === 1 ? ' was' : 's were'}`;
}
