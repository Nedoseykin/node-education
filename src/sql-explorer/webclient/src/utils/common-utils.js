function isExists(v) {
  return (v !== undefined) && (v !== null);
}

function isFilledString(v) {
  return (typeof v === 'string') && (v.length > 0);
}

function isFilledArray(v) {
  return Array.isArray(v) && (v.length > 0);
}

function uniqueArray(v) {
  let r = {};
  return v.filter(i => v.hasOwnProperty(i) ? !1 : r[i] = !0);
}

export {
  isExists,
  isFilledString,
  isFilledArray,
  uniqueArray,
};
