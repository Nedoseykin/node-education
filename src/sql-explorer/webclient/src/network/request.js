import { get } from 'lodash';
import { isFilledString } from '../utils';

const createError = (err) => ({
  message: get(err, 'message', 'Network error'),
  status: parseInt(get(err, 'status', 500), 10),
  dateTime: parseInt(get(err, 'dateTime', new Date().getTime()), 10),
});

export default ({
  url = '',
  method = 'GET',
  headers = {},
  body = null,
  onError,
  onSuccess,
}) => {
  return fetch(
    url,
    {
      method,
      headers: {
        'content-type': headers.contentType || 'application/json',
      },
      body,
    }
  )
    .then((response) => {
      const { headers: respHeaders, status } = response;
      const contentType = respHeaders.get('content-type');
      if (contentType.includes('json')) {
        return response.json();
      }
      if (contentType.includes('text')) {
        return response.text();
      }
      return status < 400;
    })
    .then((data) => {
      if (!data || isFilledString(data.message)) {
        onError && onError(createError(data));
      } else {
        onSuccess && onSuccess(data);
      }
    })
    .catch((err) => {
      onError && onError(createError(err));
    });
};
