import { handleActions } from 'redux-actions';
import { acNetworkSet, acResetApp } from '../actions';

const initialState = {
  requests: 0,
};

export default handleActions({
  [acNetworkSet]: (state, { payload }) => {
    const { increment = 0, ...rest } = payload;
    return {
      ...state,
      requests: state.requests + increment,
      ...rest,
    };
  },

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
