import { handleActions } from 'redux-actions';
import { acResetApp, acSetPayload, acSetValue } from '../actions';

const initialState = {};

export default handleActions({
  [acSetValue]: (state, { payload }) => ({
    ...state,
    [payload.name]: payload.value,
  }),

  [acSetPayload]: (state, { payload }) => ({
    ...state,
    ...payload,
  }),

  [acResetApp]: () => ({
    ...initialState,
  }),
}, initialState);
