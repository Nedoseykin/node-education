import { combineReducers } from 'redux';

import model from './model-reducer';

import network from './network-reducer';

export default combineReducers({
  model,
  network,
});
