import { createAction } from 'redux-actions';
import { AC_NETWORK_SET } from './action-types';

export const acNetworkSet = createAction(AC_NETWORK_SET);
