export const AC_RESET_APP = 'AC_RESET_APP';

export const AC_SET_VALUE = 'AC_SET_VALUE';

export const AC_SET_PAYLOAD = 'AC_SET_PAYLOAD';

export const AC_NETWORK_SET = 'AC_NETWORK_SET';
