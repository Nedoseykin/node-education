import { createAction } from 'redux-actions';
import { AC_SET_PAYLOAD, AC_SET_VALUE } from './action-types';

export const acSetValue = createAction(AC_SET_VALUE);

export const acSetPayload = createAction(AC_SET_PAYLOAD);
