import { get } from 'lodash'
import { createSelector } from 'reselect';
import { isFilledArray } from '../../utils';

export const sqlSimpleSelector = ({ model }) => get(model, 'sql', '');

export const errorsSimpleSelector = ({ model }) => get(model, 'errors', []);

export const sqlListSimpleSelector = ({ model }) => get(model, 'sqlList', []);

export const responseDataSimpleSelector = ({ model }) => get(model, 'responseData', null);

export const activeSqlIdSimpleSelector = ({ model }) => get(model, 'activeSqlId', 0);

// memoized
export const sqlListSelector = createSelector(
  [sqlListSimpleSelector],
  (sqlList) => {
    return isFilledArray(sqlList)
      ? sqlList.map((name, i) => ({ id: i + 1, name }))
      : [];
  },
);
