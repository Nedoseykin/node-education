import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Aside.module.scss';

const Aside = ({ children }) => (
  <aside className={styles.root}>
    {children}
  </aside>
);

Aside.propTypes = {
  children: PropTypes.any,
};

Aside.defaultProps = {
  children: [],
};

export default Aside;
