import React from 'react';
import * as PropTypes from 'prop-types';

import List from '../List/List';

import styles from './SqlList.module.scss';

console.log('root: ', styles.root);

const SqlList = ({
  sqlList,
  onSelect,
  onActivate,
  disabled,
  value,
}) => (
  <List
    className={styles.root}
    id="sqlList"
    label="SQL"
    itemsList={sqlList}
    disabled={disabled}
    onSelect={!disabled ? onSelect : null}
    onActivate={
      !disabled && onActivate
        ? () => onActivate(value, sqlList)
        : null
    }
    noDataElement={'No SQL was found'}
    value={value}
  />
);

SqlList.propTypes = {
  sqlList: PropTypes.arrayOf(PropTypes.shape({})),
  disabled: PropTypes.bool,
  onSelect: PropTypes.func,
  onActivate: PropTypes.func,
  value: PropTypes.number,
};

SqlList.defaultProps = {
  sqlList: [],
  disabled: false,
  onSelect: null,
  onActivate: null,
  value: 0,
};

export { SqlList };
