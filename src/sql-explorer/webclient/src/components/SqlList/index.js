import { connect } from 'react-redux';

import { SqlList } from './SqlList';

import { sqlListSelector, activeSqlIdSimpleSelector } from '../../store/selectors';
import { acSetPayload, acSetValue } from '../../store/actions';
import { isFilledArray } from '../../utils';

const onSelect = e => dispatch => (
  dispatch(acSetValue({ name: 'activeSqlId', value: parseInt(e.target.id, 10) }))
);

const onActivate = (value, sqlList) => dispatch => {
  const item = isFilledArray(sqlList) ? sqlList.find(item => item.id === value) : null;
  if (item) {
    dispatch(acSetPayload({ sql: item.name, responseData: null }));
  }
};

const mapStateToProps = state => ({
  sqlList: sqlListSelector(state),
  value: activeSqlIdSimpleSelector(state),
});

const mapDispatchToProps = {
  onSelect,
  onActivate,
};

export default connect(mapStateToProps, mapDispatchToProps)(SqlList);
