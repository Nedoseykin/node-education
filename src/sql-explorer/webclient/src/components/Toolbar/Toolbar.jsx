import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Toolbar.module.scss';

const Toolbar = ({
  sql,
  errors,
  sqlList,
  onRun,
  onClear,
}) => (
  <div className={styles.root}>
    <button
      key="clear"
      id="clear"
      className={styles.btn}
      onClick={onClear}
    >
      Clear
    </button>
    <button
      key="run"
      id="run"
      className={styles.btn}
      onClick={() => onRun(sql, errors, sqlList)}
    >
      Run
    </button>
  </div>
);

Toolbar.propTypes = {
  sql: PropTypes.string,
  errors: PropTypes.arrayOf(PropTypes.shape({})),
  sqlList: PropTypes.arrayOf(PropTypes.string),
  onRun: PropTypes.func,
  onClear: PropTypes.func,
};

Toolbar.defaultProps = {
  sql: '',
  onRun: () => {},
  onClear: () => {},
};

export { Toolbar };
