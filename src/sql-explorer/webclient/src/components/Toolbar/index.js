import { connect } from 'react-redux';

import { Toolbar } from './Toolbar';
import {
  sqlSimpleSelector,
  errorsSimpleSelector,
  sqlListSimpleSelector,
} from '../../store/selectors';
import { acSetPayload, acSetValue } from '../../store/actions';
import { request, runSqlUrl } from '../../network';

const onRun = (sql, errors, sqlList) => (dispatch) => {
  console.log('Run:', sql);
  request({
    url: runSqlUrl,
    method: 'POST',
    body: JSON.stringify({ sql }),
    onError: (err) => {
      console.error(err);
      dispatch(acSetPayload({
        errors: [...errors, err],
        responseData: null,
      }));
    },
    onSuccess: (data) => {
      console.log('Success: ', data);
      const sqlItem = sql.replace(/\s{2,}/, ' ').trim().toLowerCase();
      dispatch(acSetPayload({
        sqlList: [...new Set([...sqlList, sqlItem])],
        responseData: data,
      }));
    },
  });
};

const onClear = () => dispatch => dispatch(acSetValue({ name: 'sql', value: '' }));

const mapStateToProps = state => ({
  sql: sqlSimpleSelector(state),
  errors: errorsSimpleSelector(state),
  sqlList: sqlListSimpleSelector(state),
});

const mapDispatchToProps = {
  onRun,
  onClear,
};

export default connect(mapStateToProps, mapDispatchToProps)(Toolbar);
