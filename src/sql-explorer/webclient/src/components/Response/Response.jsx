import React from 'react';
import * as PropTypes from 'prop-types';

import ReactJson from 'react-json-view';

import { isExists } from '../../utils';

import styles from './Response.module.scss';

const Response = ({
  value,
}) => {
  let isJSON;
  try {
    isJSON = JSON.stringify(value);
    isJSON = true;
  } catch (e) {
    isJSON = false;
  }

  return isExists(value) && (
    <div className={styles.root}>
      {
        isJSON && (
          <ReactJson src={value} />
        )
      }
      {
        !isJSON && `${value}`
      }
    </div>
  )
};

Response.propTypes = {
  value: PropTypes.any,
};

Response.defaultProps = {
  value: null,
};

export { Response };
