import { connect } from 'react-redux';

import { Response } from './Response';

import { responseDataSimpleSelector } from '../../store/selectors';

const mapStateToProps = state => ({
  value: responseDataSimpleSelector(state),
});

export default connect(mapStateToProps)(Response);
