import React from 'react';
import { Provider } from 'react-redux';
import styles from './App.module.scss';

import { configureStore } from '../../store';

import Header from '../Header/Header';
import Content from '../Content/Content';
import Aside from '../Aside/Aside';
import Central from '../Central/Central';
import Form from '../Form';
import Toolbar from '../Toolbar';
import SqlList from '../SqlList';

const store = configureStore();

function App() {
  return (
    <Provider store={store}>
      <div className={styles.App}>
        <Header title="SQL Explorer" />
        <Content>
          <Aside>
            <SqlList />
          </Aside>
          <Central>
            <Form />
            <Toolbar />
          </Central>
        </Content>
      </div>
    </Provider>
  );
}

export default App;
