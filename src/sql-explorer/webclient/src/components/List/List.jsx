import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import { isFilledArray, isFilledString } from '../../utils';

import './List.scss';

const List = ({
  id,
  label,
  itemsList,
  className,
  noDataElement,
  disabled,
  value,
  onSelect,
  onActivate,
}) => {
  const listId = `${id}_list`;
  const items = isFilledArray(itemsList)
    ? itemsList.map((item) => {
      const { id: itemId, name, disabled: itemDisabled = false } = item;
      const isDisabled = disabled || itemDisabled;
      return (
        <li
          key={itemId}
          id={itemId}
          className={classNames(
            'List__item',
            itemDisabled && 'List__item_disabled',
            (value === itemId) && 'List__item_selected'
          )}
          onClick={!isDisabled ? onSelect : null}
          onDoubleClick={!isDisabled ? onActivate : null}
        >
          {name}
        </li>
      )
    })
    : noDataElement;
  return (
    <div
      id={`${id}`}
      className={classNames(
        'List',
        disabled && 'List__list_disabled',
        isFilledString(className) && className,
      )}
    >
      <label htmlFor={listId} className="List__label">
        {label}
      </label>
      <ul id={listId} className={'List__list'}>
        { items }
      </ul>
    </div>
  );
};

List.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  itemsList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    disabled: PropTypes.bool,
  })),
  disabled: PropTypes.bool,
  value: PropTypes.number,
  noDataElement: PropTypes.node,
  onSelect: PropTypes.func,
  onActivate: PropTypes.func,
};

List.defaultProps = {
  className: '',
  label: '',
  itemsList: [],
  noDataElement: null,
  disabled: false,
  value: 0,
  onSelect: null,
  onActivate: null,
};

export default List;
