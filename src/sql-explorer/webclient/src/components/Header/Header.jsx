import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Header.module.scss';

const Header = ({
  title = '',
}) => (
  <header className={styles.root}>
    <span className={styles.title}>{title}</span>
  </header>
);

Header.propTypes = {
  title: PropTypes.string,
};

Header.defaultProps = {
  title: '',
};

export default Header;
