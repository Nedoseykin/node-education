import { connect } from 'react-redux';

import { Form } from './Form';
import { sqlSimpleSelector } from '../../store/selectors';
import { acSetValue } from '../../store/actions';

const onChange = e => dispatch => dispatch(acSetValue({
  name: e.target.name,
  value: e.target.value,
}));

const mapStateToProps = state => ({
  sql: sqlSimpleSelector(state),
});

const mapDispatchToProps = {
  onChange,
};

export default connect(mapStateToProps, mapDispatchToProps)(Form);
