import React from 'react';
import * as PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Form.module.scss';

import Response from '../Response';

const Form = ({
  sql,
  onChange,
}) => (
  <form name="form" className={styles.root}>
    <div key="sql" className={styles.row}>
      <textarea
        className={classNames(styles.control, styles.sqlInput)}
        name="sql"
        id="sql"
        cols="30"
        rows="10"
        value={sql}
        onChange={onChange}
      />
    </div>
    <div key="responseData" className={classNames(styles.row, styles.responseRow)}>
      <Response />
    </div>
  </form>
);

Form.propTypes = {
  sql: PropTypes.string,
  onChange: PropTypes.func,
};

Form.defaultProps = {
  sql: '',
  onChange: () => {},
};

export { Form };
