import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Content.module.scss';

const Content = ({
  children,
}) => (
  <div className={styles.root}>
    { children }
  </div>
);

Content.propTypes = {
  children: PropTypes.any,
};

Content.defaultProps = {
  children: [],
};

export default Content;
