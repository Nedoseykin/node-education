import React from 'react';
import * as PropTypes from 'prop-types';

import styles from './Central.module.scss';

const Central = ({ children }) => (
  <section className={styles.root}>
    {children}
  </section>
);

Central.propTypes = {
  children: PropTypes.any,
};

Central.defaultProps = {
  children: [],
};

export default Central;
