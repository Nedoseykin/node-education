class BasicError extends Error {
  constructor(message, status) {
    super(message);
    this.status = status || 500;
    this.dateTime = new Date().getTime();
    this.toObj = this.toObj.bind(this);
  }

  toObj = function toObj() {
    return {
      message: this.message,
      dateTime: this.dateTime,
      status: this.status,
    };
  };

  toStr = function toStr() {
    return [this.dateTime, this.status, this.message].join(' - ');
  }
}

module.exports = BasicError;
