function isExists(v) {
  return (v !== undefined) && (v !== null);
}

function isFilledString(v) {
  return (typeof v === 'string') && (v.length > 0);
}

function isFilledArray(v) {
  return Array.isArray(v) && (v.length > 0);
}

module.exports = {
  isExists,
  isFilledString,
  isFilledArray,
};
