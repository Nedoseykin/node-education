const fs = require('fs');

const os = require('os');

const { isFilledString } = require('./common-utils');

const appendLineToFile = (fileName, line, options = {}) => {
  const {
    withEOL = true,
    existsOnly = false,
    filledOnly = true,
  } = options;
  if (filledOnly && !isFilledString(line)) {
    return { message: 'String is empty.', dateTime: new Date().getTime() };
  }
  const isExists = fs.existsSync(fileName);
  let action = 'appendFile';
  if (!isExists) {
    if (existsOnly) {
      return { message: 'File does not exist.', dateTime: new Date().getTime() };
    }
    action = 'writeFile';
  }
  fs[action](fileName, `${line}${withEOL ? os.EOL : ''}`, 'utf-8', (err) => {
    if (err) {
      return { message: `Can not write to file: ${fileName}`, dateTime: new Date().getTime() };
    } else {
      return true;
    }
  });
};

const mkDirIfNotExistsSync = (dirName) => {
  if (fs.existsSync(dirName)) {
    return false;
  } else {
    try {
      fs.mkdirSync(dirName, { recursive: true });
      return true;
    } catch (e) {
      return { message: e.message };
    }
  }
};

const mkDirIfNotExists = (dirName) => new Promise((resolve, reject) => {
  if (fs.existsSync(dirName)) {
    resolve(false);
  } else {
    fs.mkdir(dirName, { recursive: true }, (err) => {
      if (err) {
        reject(err);
      } else {
        resolve(true);
      }
    });
  }
});

module.exports = {
  appendLineToFile,
  mkDirIfNotExistsSync,
  mkDirIfNotExists,
};
