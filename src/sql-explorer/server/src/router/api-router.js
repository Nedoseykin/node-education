const apiRouter = require('express').Router();

const v1 = require('../controller/v1/api-controller');

apiRouter.post('/v1/run-sql', v1.runSqlController);

module.exports = apiRouter;
