const path = require('path');

const { dataBase } = require('../../../configs/server');

const { dao: daoName } = require('../../../configs/dao')[dataBase];

const daoPath = path.resolve(__dirname, '..', '..', 'dao', daoName);

const dao = require(daoPath);

const BasicError = require('../../error/BasicError');

const log = require('../../logger/logger');

const getResponseString = (data) => {
  if (Array.isArray(data)) {
    return `Array, length: ${data.length}`;
  } else if (typeof data === 'string') {
    return data;
  } else {
    return `${data}`;
  }
};

exports.runSqlController = (req, res) => {
  const { sql } = req.body;
  log(`body.sql: "${sql}"`);
  dao.runSql(sql)
    .then((data) => {
      console.log('console: data: ', data);
      log(`Request: successful. Data: ${getResponseString(data)}`);
      res.status(200).send(data);
    })
    .catch((err) => {
      const data = !(err instanceof BasicError)
        ? new BasicError(err.message)
        : err;
      const { status } = data;
      log(data.toStr(), false);
      res.status(status).send(data.toObj());
    });
};
