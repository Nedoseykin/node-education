const path = require('path');

const config = require('../../configs/server');

const { isFilledString } = require('../utils/common-utils');

const { appendLineToFile } = require('../utils/fs-utils');

const { logFileName = 'server.log' } = config;

const dirName = path.resolve(__dirname, '..', '..', 'logs');

const fileName = path.resolve(dirName, logFileName);

const log = (message, withData = true) => {
  const date = withData ? `${new Date().getTime()}` : '';
  const str = [date, message].filter(isFilledString).join(' - ');
  console.log(str);
  appendLineToFile(fileName, str);
};

module.exports = log;
