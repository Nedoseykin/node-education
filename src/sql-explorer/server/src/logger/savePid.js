const path = require('path');

const fs = require('fs');

const config = require('../../configs/server');

const {
  mkDirIfNotExistsSync,
} = require('../utils/fs-utils');

const { pidFileName = 'server.pid' } = config;

const dirName = path.resolve(__dirname, '..', '..', 'logs');

const fileName = path.resolve(dirName, pidFileName);

const savePidSync = (pid) => {
  try {
    mkDirIfNotExistsSync(dirName);
    fs.writeFileSync(fileName, `${pid}`, 'utf-8');
  } catch (e) {
    console.log(e.message);
  }
};

module.exports = savePidSync;
