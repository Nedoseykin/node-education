const mysql = require('mysql');

const { isFilledString } = require('../utils/common-utils');

const BasicError = require('../error/BasicError');

const log = require('../logger/logger');

const query = async (sql, connection) => {
  return new Promise((resolve, reject) => {
    connection.query(sql, (err, results) => {
      if (err) {
        reject(new BasicError(err.message, 400));
      } else {
        resolve(results);
      }
    });
  });
};

const runSql = async (sql) => {
  log(`DAO Mysql: received sql: "${sql}"`);
  if (!isFilledString(sql)) {
    throw new BasicError('SQL is empty', 400);
  }
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'nodeUser',
    password: 'nodePassword',
    database: 'books',
  });
  connection.connect();
  const data = await query(sql, connection);
  connection.end();
  return data;
};

module.exports = {
  runSql,
};
