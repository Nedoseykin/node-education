const path = require('path');

const express = require('express');

const bodyParser = require('body-parser');

const helmet = require('helmet');

const config = require('./configs/server');

const log = require('./src/logger/logger');

const savePid = require('./src/logger/savePid');

const BasicError = require('./src/error/BasicError');

const apiRouter = require('./src/router/api-router');

const port = config.port;

const { pid } = process;

const mode = process.env.NODE_ENV || 'development';

savePid(pid);
console.log('');
log('---------------------------------------------', false);
log(`Application was started with pid: ${pid} in ${mode} mode.`);

const server = express();

process.on('SIGTERM', () => {
  console.log('Process terminated');
  process.exit(0);
});

server.set('etag', 'strong');

server.use(helmet());

server.use((req, res, next) => {
  const { method, originalUrl } = req;
  log(`${method} - ${originalUrl}`);
  next();
});

server.use(bodyParser.urlencoded({ extended: true }));

server.use(bodyParser.json());

server.use(bodyParser.text());

server.use('/api', apiRouter);

server.use('/', express.static(path.resolve(__dirname, 'public')));

server.use((req, res) => {
  const message = 'Resource was not found';
  const status = 404;
  const errObj = new BasicError(message, status);
  log(errObj.toStr(), false);
  res.status(status).json(errObj.toObj());
});

server.listen(port, () => {
  log(`Server listens on port: ${port}`);
});
