const path = require('path');

const fs = require('fs');

const config = require('./configs/server');

const { pidFileName } = config;

const fileName = path.resolve(__dirname, 'logs', pidFileName);

try {
  const str = fs.readFileSync(fileName, 'utf-8');
  const pid = parseInt(str, 10);
  if (!isNaN(pid)) {
    process.kill(pid, 'SIGTERM');
  }
} catch (e) {
  console.error(`Error: can not kill: ${e.message}`);
}
