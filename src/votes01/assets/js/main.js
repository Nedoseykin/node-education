const formName = 'votesForm';
const radioGroupName = 'value';
const STATISTICS_DOWNLOAD_BUTTON_TYPES = [ 'XML', 'HTML', 'JSON' ];

document.addEventListener('DOMContentLoaded', start);

async function start() {
  const variants = await getVariants();
  const votesStatistics = await getVotesStatistics();

  if (Array.isArray(variants)) {
    renderVoteForm(variants);

    if (votesStatistics) {
      showStatistics(votesStatistics, variants);
      renderStatisticsToolbar();
      const modalCloseButton = document.getElementById('statistics__modal__close_button');
      modalCloseButton.addEventListener('click', closeStatisticsModalView);
    }
  }
}

// updaters
function showStatistics(votesStatistics, variants) {
  const statisticsContent = document.getElementById('statistics__content');
  statisticsContent.innerHTML = '';

  let sum = 0;

  if (Array.isArray(variants)) {
    variants.forEach(v => {
      const { id, name } = v;
      const count = (id in votesStatistics)
        ? votesStatistics[id]
        : 0;
      sum += count;
      const span = getElement(
        'span',
        { id: `vote_${id}`, class: 'vote__item' },
        `${name}: ${count}`,
      );
      statisticsContent.appendChild(span);
    });
  }

  const totalCount = document.getElementById('total__count');
  totalCount.innerHTML = sum;
}

function showStatisticsModalView(response) {
  const modalView = document.getElementById('statistic__modal_view');
  const modalContent = modalView.querySelector('#statistic__modal_content');
  modalContent.innerHTML = '';
  const code = getElement('code');
  code.innerText = response;
  // modalContent.innerText = response;
  modalContent.appendChild(code);
  modalView.classList.add('modal__visible');
}

// element creators
function getElement(tag, attributes, innerHtml) {
  const elm = document.createElement(tag);
  if (attributes) {
    Object.keys(attributes)
      .forEach(name => {
        const value = attributes[name];
        elm.setAttribute(name, value);
      });
  }
  if (innerHtml) { elm.innerHTML = innerHtml; }
  return elm;
}

function renderVoteForm(variants) {
  const wrapper = document.getElementById('wrapper');
  wrapper.innerHTML = '';
  const form = getElement('form', {
    name: formName,
    method: 'post',
    action: '/vote',
  });
  wrapper.appendChild(form);
  variants.forEach(variant => {
    const { id, name } = variant;
    const controlBox = getFormControlBox();
    const label = getElement('label', { 'for': id }, name);
    const input = getElement('input', { id, value: id, name: radioGroupName, type: 'radio' });
    controlBox.appendChild(label);
    controlBox.appendChild(input);
    form.appendChild(controlBox);
  });
  const submit = getVoteSubmit();
  const controlBox = getFormControlBox(['btn__submit']);
  controlBox.appendChild(submit);
  form.appendChild(controlBox);
  wrapper.appendChild(form);
}

function getFormControlBox(addClasses) {
  let classes = ['form__control'];
  if (Array.isArray(addClasses)) {
    classes = classes.concat(addClasses);
  }
  classes = classes.join(' ');
  return getElement('div', { class: classes });
}

function getVoteSubmit() {
  return getElement('input', {
    type: 'submit',
    value: 'Submit',
  })
}

function renderStatisticsToolbar() {
  const showToolbar = document.getElementById('statistics__toolbar_show');
  showToolbar.innerHTML = '';
  const downloadToolbar = document.getElementById('statistics__toolbar_download');
  downloadToolbar.innerHTML = '';
  STATISTICS_DOWNLOAD_BUTTON_TYPES.forEach((buttonType) => {
    const showButton = getStatisticsShowButton(buttonType);
    showToolbar.appendChild(showButton);
    const downloadButton = getStatisticsDownloadButton(buttonType);
    downloadToolbar.appendChild(downloadButton);
  });
}

function getStatisticsShowButton(buttonType) {
  const {
    attributes,
    innerHTML,
    onClick,
  } = getStatisticsShowButtonOptions(buttonType);
  const button = getElement('button', attributes, innerHTML);
  button.addEventListener('click', onClick);
  return button;
}

function getStatisticsDownloadButton(buttonType) {
  const {
    id,
    form: formAttributes,
    inputSubmit: inputSubmitAttributes,
    inputType: inputTypeAttributes,
  } = getStatisticsDownloadButtonOptions(buttonType);
  const form = getElement('form', formAttributes);
  const inputType = getElement('input', inputTypeAttributes);
  const submitButton = getElement('input', inputSubmitAttributes);
  form.appendChild(inputType);
  form.appendChild(submitButton);
  const formWrapper = getElement(
    'div',
    {
      id: `btnDownload${id}`,
      class: 'btn__download',
    },
  );
  formWrapper.appendChild(form);
  return formWrapper;
}

// helpers
function getStatisticsShowButtonOptions(buttonTypeParam) {
  const buttonType = buttonTypeParam.toLowerCase();
  return {
    attributes: {
      id: `buttonShow${buttonTypeParam}`,
      class: 'btn__show',
    },
    innerHTML: `Show ${buttonType}`,
    onClick: handleShowStatisticsClick(buttonType),
  }
}

function getStatisticsDownloadButtonOptions(buttonType) {
  return {
    id: buttonType,
    inputType: {
      name: 'type',
      type: 'hidden',
      value: buttonType.toLowerCase(),
    },
    inputSubmit: {
      type: 'submit',
      value: `Download ${buttonType}`,
    },
    form: {
      name: `formBtn${buttonType}`,
      method: 'GET',
      action: '/statistics/download',
    },
  }
}

// handlers
function handleShowStatisticsClick(type) {
  const TYPES = {
    json: getStatisticsByContentType('application/json'),
    xml: getStatisticsByContentType('text/xml; charset=UTF-8'),
    html: getStatisticsByContentType('text/html; charset=UTF-8'),
  };
  const request = TYPES[type];
  if (request) {
    return async function() {
      const response = await request();
      showStatisticsModalView(response);
    }
  } else {
    return () => {console.log('Handler is not defined: ', type)};
  }
}

function closeStatisticsModalView() {
  const modalView = document.getElementById('statistic__modal_view');
  modalView.classList.remove('modal__visible');
}

// requests
async function request(url, options = {}, data = null) {
  const applicationJson = 'application/json';
  const textPlain = 'text/plain';

  const {
    method = 'GET',
    headers = {},
  } = options;

  const {
    contentType = applicationJson,
    ...restHeaders
  } = headers;

  let body = null;
  if (data) {
    if (contentType === applicationJson) {
      body = JSON.stringify(data);
    }
    if (contentType === textPlain) {
      body = `${data}`;
    }
  }

  return  await fetch(
    url,
    {
      method,
      headers: {
        'Content-Type': contentType,
        ...restHeaders
      },
      body,
    },
  );
}

async function getVariants() {
  const response = await request('/variants');
  try {
    const data = await response.json();
    return Promise.resolve(data);
  } catch (e) {
    return Promise.resolve([]);
  }
}

async function getVotesStatistics() {
  const fetchOptions = {
    headers: {
      'Accept': 'application/json; charset=UTF-8',
      'Content-Type': 'application/json',
    },
  };
  const response = await request('/statistics/common', fetchOptions);
  try {
    const data = await response.json();
    return Promise.resolve(data);
  } catch (e) {
    return Promise.resolve(null);
  }
}

function getStatisticsByContentType(contentType) {
  return async function() {
    const requestOptions = {
      headers: {
        'Accept': contentType,
        'Content-Type': contentType,
      }
    };
    const response = await request('/statistics/by-type', requestOptions);
    if (response.ok) {
      return await response.text();
    } else {
      return Promise.resolve('Error');
    }
  };
}
