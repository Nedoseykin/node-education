const { readVariants } = require('../dao/variants');
const { readVotes, saveVotes } = require('../dao/votes');
const { log } = require('../helpers/log');
const Vote = require('../domain/Vote');

async function postVoteService(req, res) {
  let { value } = req.body;
  if ((typeof value === 'string') && value.length > 0) {
    const variants = await readVariants();
    const index = Array.isArray(variants)
      ? variants.findIndex(v => v.id === value)
      : -1;
    if (index === -1) {
      log(`Error - postVoteService - wrong value: ${value}`);
      return res.send(false);
    }
    const votes = await readVotes();
    votes.push(new Vote(value));
    const result = await saveVotes(votes);
    log(`${result
      ? 'Success - postVoteService'
      : 'Error - postVoteService'
    }`);
  }
  return res.redirect('/');
}

module.exports = {
  postVoteService,
};
