const os = require('os');
const { sha256 } = require('js-sha256');
const { readVotes } = require('../dao/votes');
const { readVariants } = require('../dao/variants');
const { log } = require('../helpers/log');

const TYPES = {
  json: {
    contentType: 'application/json',
    getStatistics: getJson,
  },
  xml: {
    contentType: 'text/xml; charset=UTF-8',
    getStatistics: getXml,
  },
  html: {
    contentType: 'text/html; charset=UTF-8',
    getStatistics: getHtml,
  },
  txt: {
    contentType: 'text/plain; charset=UTF-8',
    getStatistics: getTxt,
  }
};

function getJson(variantsStatistics) {
  let statistics = null;
  let message = 'Unable to create JSON data - variants statistics is not available';

  if (variantsStatistics) {
    statistics = JSON.stringify(variantsStatistics, null, 2);
    message = 'JSON data of variants statistics has been created';
  }

  log(message);
  return statistics;
}

function getXml(variantsStatistics) {
  let statistics = null;
  let message = 'Unable to create XML data - variants statistics is not available';

  if (variantsStatistics) {
    statistics = '<Statistics>' + os.EOL;
    Object.keys(variantsStatistics)
      .forEach(variant => {
        statistics += '  <Vote>' + os.EOL;
        statistics += `    <Name>${variant}</Name>${os.EOL}`;
        statistics += `    <Count>${variantsStatistics[variant]}</Count>${os.EOL}`;
        statistics += '  </Vote>' + os.EOL;
      });
    statistics += '</Statistics>';
    message = 'XML data of variants statistics has been created';
  }

  log(message);
  return statistics;
}

function getHtml(variantsStatistics) {
  let statistics = null;
  let message = 'Unable to create HTML data - variants statistics is not available';

  if (variantsStatistics) {
    statistics = '<div>' + os.EOL;
    Object.keys(variantsStatistics)
      .forEach(variant => {
        statistics += `  <p>${variant}: ${variantsStatistics[variant]}</p>${os.EOL}`;
      });
    statistics += '</div>';
    message = 'HTML data of variants statistics has been created';
  }

  log(message);
  return statistics;
}

function getTxt() {
  return null;
}

// services
async function getStatisticsVotesService(req, res) {
  try {
    const votesStatistics = await getVotesStatistics();

    if (votesStatistics) {
      const etag = sha256(JSON.stringify(votesStatistics));
      const ifNoneMatch = req.headers['if-none-match'];
      const status = etag === ifNoneMatch ? 304 : 200;

      if (status === 200) {
        log('200 - Votes statistics has been sent. Data was changed.');

        res
          .status(status)
          .set({
            'Content-Type': 'application/json; charset=UTF-8',
            'ETag': etag,
            'Cache-Control': 'no-cache, max-age=0',
          })
          .send(votesStatistics);
      } else if (status === 304) {
        log('304 - Status has been sent. Data was not changed.');
        res.sendStatus(status);
      }
      return Promise.resolve(votesStatistics);
    }
  } catch (e) {
    return Promise.resolve(statisticsErrorService(req, res, e));
  }
}

async function statisticsDownloadService(req, res) {
  try {
    const { type = 'txt' } = req.query;

    log(`GET - statistics download - type of file is ${type}`);

    const contentDisposition = `attachment; filename=statistics.${type}`;

    res.locals = {
      type,
      contentDisposition,
    };

    return await sendStatisticsByTypeService(req, res);
  } catch (e) {
    console.log(e);
  }
}

async function statisticsGetByContentTypeService(req, res) {
  try {
    const { 'accept': requestContentType } = req.headers;

    log(`GET - request header Accept: ${requestContentType}`);
    const typeParam = getTypeParam(requestContentType);

    res.locals = {
      type: typeParam,
    };

    return await sendStatisticsByTypeService(req, res);
  } catch (e) {
    console.log(e);
  }
}

async function sendStatisticsByTypeService(req, res) {
  try {
    const {
      type,
      contentDisposition,
    } = res.locals;

    const {
      statistics,
      contentType,
    } = await getStatisticsParams(type);

    if (statistics) {
      const etag = sha256(JSON.stringify(statistics));
      const ifNoneMatch = req.headers['if-none-match'];
      const status = etag === ifNoneMatch ? 304 : 200;



      if (status === 200) {
        const responseHeaders = {
          'Content-Type': contentType,
          'ETag': etag,
          'Cache-Control': 'no-cache, max-age=0',
        };

        if (contentDisposition) {
          responseHeaders['Content-Disposition'] = contentDisposition;
        }
        log(`200 - Variants statistics has been sent as ${contentType}. Data was changed.`);
        res
          .status(status)
          .set(responseHeaders)
          .send(statistics);
      } else if (status === 304) {
        log('304 - Status has been sent. Data was not changed.');
        res.sendStatus(status);
      }
      return Promise.resolve(statistics);
    }
  } catch (e) {
    return Promise.resolve(statisticsErrorService(req, res, e));
  }
}

function statisticsErrorService(req, res, e) {
  const error = { errorMessage: 'Statistics data is not available' };

  log(`409 - Statistics data is not available, ${e}`);

  res
    .status(409)
    .set({
      'Content-Type': 'application/json; charset=UTF-8',
      'Cache-Control': 'no-cache, no-store, max-age=0',
    })
    .send(error);
  return error;
}

// helpers
async function getVotesStatistics() {
  const votes = await readVotes();
  return calculateVotesStatistics(votes);
}

function calculateVotesStatistics (votes) {
  let votesStatistics = null;
  let message = 'Unable to calculate votes` statistics - no votes data is collected';
  if (Array.isArray(votes)) {
    votesStatistics = votes.reduce(
      (accumulator, currentValue) => {
        const { value } = currentValue;
        accumulator[value] = accumulator[value] ? accumulator[value] + 1 : 1;
        return accumulator;
      },
      {},
    );
    message = `Statistics of votes is calculated: ${JSON.stringify(votesStatistics)}`;
  }

  log(message);
  return votesStatistics;
}

async function getVariantsStatistics(votesStatistics) {
  const variants = await readVariants();
  return calculateVariantsStatistics(variants, votesStatistics);
}

function calculateVariantsStatistics(variants, votesStatistics) {
  let variantsStatistics = null;
  let message = 'Unable to calculate variants` statistics - no votes statistics data is collected';

  if (Array.isArray(variants) && votesStatistics) {
    variantsStatistics = {};
    variants.forEach(v => {
      const { id, name } = v;
      if (id in votesStatistics) {
        variantsStatistics[name] = votesStatistics[id];
      }
    });
    message = `Statistics of variants is calculated: ${JSON.stringify(variantsStatistics)}`
  }

  log(message);
  return variantsStatistics;
}

function getTypeParam(contentType) {
  if (contentType.indexOf('xml') > -1) {
    return 'xml';
  }
  if (contentType.indexOf('html') > -1) {
    return 'html';
  }
  if (contentType.indexOf('json') > -1) {
    return 'json';
  }
  return 'txt';
}

async function getStatisticsParams(typeParam) {
  const type = typeParam in TYPES ? typeParam : 'txt';

  const {
    contentType,
    getStatistics,
  } = TYPES[type];

  const votesStatistics = await getVotesStatistics();
  const variantsStatistics = await getVariantsStatistics(votesStatistics);
  const statistics = getStatistics(variantsStatistics);

  return {
    contentType,
    statistics,
  }
}

module.exports = {
  getStatisticsVotesService,
  statisticsDownloadService,
  statisticsGetByContentTypeService,
};
