const { readVariants } = require('../dao/variants');

async function getVariantsService(req, res) {
  const variants = await readVariants();
  res.send(variants);
}

module.exports = {
  getVariantsService,
};
