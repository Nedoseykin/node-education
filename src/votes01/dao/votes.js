const fs = require('fs');
const path = require('path');

const { log } = require('../helpers/log');

const votesFileName = path.resolve(__dirname, '..', 'votes.json');

async function readVotes() {
  return new Promise((resolve, reject) => {
    if (fs.existsSync(votesFileName)) {
      fs.readFile(votesFileName, 'utf-8', (err, data) => {
        if (err) {
          log(`Error - readVotes - ${err}`);
          reject(err);
        } else {
          let votes = [];
          try {
            votes = JSON.parse(data);
            log(`Success - readVotes - Array with length: ${votes.length}`);
            resolve(votes);
          } catch (e) {
            log(`Error - readVotes - ${e}`);
            reject(e);
          }
        }
      });
    } else {
      log('Success - readVotes - empty Array');
      resolve([]);
    }
  });
}

async function saveVotes(votes) {
  return new Promise((resolve, reject) => {
    let data;
    try {
      data = JSON.stringify(votes, null, 2);
      fs.writeFile(votesFileName, data, 'utf-8', (err) => {
        if (err) {
          log(`Error - saveVotes - ${err}`);
          reject(err);
        } else {
          log('Success - saveVotes - saved');
          resolve(true);
        }
      });
    } catch (e) {
      log(`Error - saveVotes - ${e}`);
      reject(e);
    }
  });
}

module.exports = {
  readVotes,
  saveVotes,
};
