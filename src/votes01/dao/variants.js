const fs = require('fs');
const path = require('path');

const { log } = require('../helpers/log');

const variantsFileName = path.resolve(__dirname, '..', 'variants.json');

async function readVariants() {
  return new Promise((resolve, reject) => {
    fs.readFile(variantsFileName, 'utf-8', (err, data) => {
      if (err) {
        log(`Error - readVariants - ${err}`);
        reject(err);
      } else {
        let variants;
        try {
          variants = JSON.parse(data);
          log(`Success - readVariants - ${
            Array.isArray(variants)
              ? `Array with length ${variants.length}`
              : 'variants'
          }`);
          resolve(variants);
        } catch (e) {
          log(`Error - readVariants - ${e}`);
          reject(e);
        }
      }
    });
  });
}

module.exports = {
  readVariants,
};
