const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const { log } = require('./helpers/log');
const { getVariantsService } = require('./service/variants');
const {
  postVoteService,
} = require('./service/votes');
const {
  getStatisticsVotesService,
  statisticsDownloadService,
  statisticsGetByContentTypeService,
} = require('./service/statistics');

const pidFileName = path.resolve(__dirname, 'server.pid');

const port = 8000;

const server = express();

const { pid } = process;

console.log('pid: ', pid);
savePid(pid, pidFileName);

server.use('/static', express.static(path.resolve(__dirname,'assets')));

server.use(bodyParser.json());

server.use(express.urlencoded({ extended: true }));

server.use(logger);

server.get('/', voteFormService);

server.get('/variants', getVariantsService);

server.post('/vote', postVoteService);

server.get('/statistics/common', getStatisticsVotesService);

server.get('/statistics/by-type', statisticsGetByContentTypeService);

server.get('/statistics/download', statisticsDownloadService);

server.listen(port, () => {
  const message = `Server listens on port ${port}`;
  log(message);
});

function voteFormService(req, res) {
  res.type('html').sendFile(path.resolve(__dirname, 'assets', 'index.html'));
}

// middleware
function logger(req, res, next) {
  const {method, originalUrl} = req;
  log(`${method} - ${originalUrl}`);
  next();
}

function savePid(pid, fileName) {
  fs.writeFile(fileName, `pid=${pid}`, (err) => {
    if (err) { throw new Error(err); }
    log('Success: process pid has been saved');
  });
}
