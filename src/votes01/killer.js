const fs = require('fs');
const path = require('path');
const pidFileName = path.resolve(__dirname, 'server.pid');
const params = process.argv.slice(2);

let paramPid = params.find(item => item.indexOf('pid=') === 0);

killServer(paramPid)
  .then((data) => { console.log(data); })
  .catch((err) => { console.log(err); });

async function killServer(paramPid) {
  let pid;

  if (!paramPid) {
    paramPid = await getPid(pidFileName);
  }

  pid = parseInt(paramPid.split('=')[1], 10);

  if (isNaN(pid)) {
    console.error('Error: pid is not a number');
    process.exit(1);
  }

  console.log('Try to kill the process: ', pid);

  try {
    process.kill(pid);
    return Promise.resolve('Success');
  } catch(err) {
    return Promise.reject(err)
  }

}

// helpers
async function getPid(fileName) {
  return await new Promise((resolve, reject) => {
    fs.readFile(fileName, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}