const fs = require('fs');
const path = require('path');
const { EOL } = require('os');

const logFileName = path.resolve(__dirname, '..','server.log');

function log(message) {
  const current = new Date();
  const localeDateTime = `${current.toLocaleDateString()}, ${current.toLocaleTimeString()}`;
  const editedMessage = `${localeDateTime} - ${message}`;
  console.log(editedMessage);
  fs.appendFileSync(logFileName, `${editedMessage}${EOL}`, 'utf-8');
}

module.exports = {
  log,
};
