const readLine = require('readline');
const fs = require('fs');
const path = require('path');
const zlib = require('zlib');

const isFilledArray = require('./src/utils/isFilledArray');

const { DEVELOPMENT } = require('./src/constants/envModes');

const mode = process.env.NODE_ENV || DEVELOPMENT;

const rl = readLine.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on('close', () => {
  rl.write('Auto-archiver would be closed\n');
  process.exit(0);
});

rl.write(`Auto-archiver is running in ${mode} mode\n`);

askQuestion(rl);

// functions
function askQuestion(rlObj) {
  rlObj.question('Please, enter the path to folder: ', getAnswer(rlObj));
}

function getAnswer(rlObj) {
  return (answer) => {
    if (!answer) { rlObj.close(); }

    rlObj.write(`Path to directory: ${answer}\n`);

    const targetDirName = getTargetDirName(answer);

    let fileNames = getFileNames(targetDirName, rl);

    const fileObjects = getFileObjects(fileNames, targetDirName); // fileNames

    if (!fileObjects) return askQuestion(rlObj);

    if (!isFilledArray(fileObjects)) {
      rlObj.write(`Directory ${answer} does not contain files for processing\n\n`);
      return askQuestion(rlObj);
    }

    processFiles(fileObjects, targetDirName, rlObj);
  };
}

function getTargetDirName(name) {
  if (name.indexOf('.') === 0) {
    return path.resolve(__dirname, name)
  }
  return name;
}

function getFileNames(targetDirName, rlObj) {
  const isExists = fs.existsSync(targetDirName);
  if (!isExists) {
    rlObj.write(`Directory ${targetDirName} was not found\n\n`);
    return null;
  }
  const targetDirStats = fs.statSync(targetDirName);
  if (!targetDirStats.isDirectory()) {
    rlObj.write(`Object ${targetDirName} is not a directory\n\n`);
    return null;
  }
  return  fs.readdirSync(targetDirName);
}

function getFileObjects(fileNames = [], targetDirName) {
  if (!fileNames) return fileNames;
  const names = Array.isArray(fileNames) ? fileNames : [];
  return  names
    .map((name) => {
      const fullName = path.resolve(targetDirName, name);
      const fileStats = fs.statSync(fullName);
      return {
        name,
        fullName,
        ext: path.extname(name),
        isDirectory: fileStats.isDirectory(),
        birthTime: fileStats.birthtimeMs,
        mTime: fileStats.mtimeMs,
      };
    })
    .filter(file => (file.isDirectory === false) && (file.ext !== '.gz'));
}

function processFiles(fileObjects, targetDirName, rlObj) {
  let totalCount = 0;
  let createdCount = 0;

  rlObj.write(getProgressString(createdCount, totalCount));

  fileObjects.forEach((file) => {
    const { name, fullName, mTime } = file;
    const archFileName = `${name}.gz`;
    const archFullFileName = path.resolve(targetDirName, archFileName);
    let shouldBeProcessed = !fs.existsSync(archFullFileName);
    if (!shouldBeProcessed) {
      const archStats = fs.statSync(archFullFileName);
      shouldBeProcessed = archStats.mtimeMs < mTime;
    }
    if (shouldBeProcessed) {
      const gzip = zlib.createGzip();
      const inp = fs.createReadStream(fullName);
      const out = fs.createWriteStream(archFullFileName);
      inp
        .pipe(gzip)
        .pipe(out)
        .on('close', () => {
          createdCount++;
          totalCount++;
          evaluate(createdCount, totalCount, fileObjects.length, rlObj);
        });
    } else {
      totalCount++;
      evaluate(createdCount, totalCount, fileObjects.length, rlObj);
    }
  });
}

function evaluate(createdCount, totalCount, length, rlObj) {
  rlObj.write(null, { ctrl: true, name: 'u' });
  rlObj.write(getProgressString(createdCount, totalCount));
  if (totalCount === length) {
    rlObj.write('\n\n');
    askQuestion(rlObj);
  }
}

function getProgressString(createdCount, processedCount) {
  const getStr = count => `${count} file${count === 1 ? ' was' : 's were'}`;
  return `Progress: ${getStr(createdCount)} created / ${processedCount} processed.`;
}
