module.exports = function isFilledArray(value) {
  return Array.isArray(value) && value.length > 0;
};
