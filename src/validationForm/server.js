const express = require('express');

const port = 8000;
const LOGIN = 'l'; // 'NodeJSUser';
const PASSWORD = 'p'; // 'NodeJSPassword';

const URL = {
  root: '/',
  error: '/404'
};

const app = express();

app.use('*', logger);

app.get(URL.root, (req, res) => {
  const { query } = req;
  const credentials = getCredentials(query);
  const isLogged = checkCredentials(credentials);
  res.locals.login = credentials.login;
  if (isLogged) {
    sendHelloService(res);
  } else {
    sendLoginFormService(res);
  }
});

app.get(URL.error, (req, res) => {
  res.status(404).send(getErrorPage());
});

app.get('*', (req, res) => {
  res.redirect(URL.error);
});



app.listen(port, () => {
  log(`Server listens at port ${port}`);
});

// middleware
function logger(req, res, next) {
  const { originalUrl, method } = req;
  log(`${method} - ${originalUrl} - request`);
  next();
}

// -- services
function sendHelloService(res) {
  const { login } = res.locals;
  log(`response - 200 - success: send hello page, login: ${login}`);
  res
    .status(200)
    .send(getHelloPage(login));
}

function sendLoginFormService(res) {
  const { login } = res.locals;
  let status = 200;
  let message = 'response - 200 - login form was sent';
  if (isNotEmptyString(login)) {
    status = 401;
    message = `response - 401 - authorization still required, login ${login}`;
  }
  log(message);
  res
    .status(status)
    .send(getLoginForm(login));
}

// -- helpers --
function log(message) {
  const currentDate = new Date().toUTCString();
  return console.log(`${currentDate} - ${message}`);
}

function getCredentials(query) {
  const login = query && isNotEmptyString(query.login) ? query.login : '';
  const password = query && isNotEmptyString(query.password) ? query.password : '';
  return { login, password };
}

function checkCredentials({ login = '', password = '' }) {
  return (login === LOGIN) && (password === PASSWORD);
}

function getHelloPage(login) {
  return `
    <h3 style="color: navy;">Hello, ${login}!</h3>
    <br>
    <a href="${URL.root}">log out</a>
  `;
}

function getLoginForm(login) {
  return(`
    <h3 style="color: ${isNotEmptyString(login) ? 'red' : 'black'}">
        Please, enter your login and password
    </h3>
    <hr>
    <form name="loginForm" method="get" action="${URL.root}">
        <div style="padding: 1em 0 0;">
            <label style="display: block; width: 150px" for="login">Login </label>
            <input type="text" id="login" name="login" value="${isNotEmptyString(login) ? login : ''}">
        </div>
        <div style="padding: 1em 0 0;">
            <label style="display: block; width: 150px" for="password">Password </label>
            <input type="password" id="password" name="password">
        </div>
        <div style="padding: 1em 0 0;">
            <input type="submit" value="Submit">
        </div>
        
    </form>
  `);
}

function getErrorPage() {
  return `
    <h3 style="color: red;">
      Error 404. Something went wrong. The page was not found.
    </h3>
  `;
}

function isExists(v) {
  return v !== undefined && v !== null;
}

function isNotEmptyString(v) {
  return isExists(v) && (typeof v === 'string') && v.length > 0;
}
