-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               8.0.16 - MySQL Community Server - GPL
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.3.0.5771
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица books.authors
CREATE TABLE IF NOT EXISTS `authors` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) DEFAULT NULL,
  `middleName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `birthday` timestamp NULL DEFAULT NULL,
  `deathday` timestamp NULL DEFAULT NULL,
  `country` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `countryKey` (`country`),
  CONSTRAINT `countryKey` FOREIGN KEY (`country`) REFERENCES `countries` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы books.authors: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
REPLACE INTO `authors` (`id`, `firstName`, `middleName`, `lastName`, `birthday`, `deathday`, `country`) VALUES
	(1, 'Kyle', NULL, 'Simpson', '1997-03-11 00:00:00', NULL, 3);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;

-- Дамп структуры для таблица books.author_of_book
CREATE TABLE IF NOT EXISTS `author_of_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `authorId` int(11) unsigned NOT NULL,
  `bookId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `authorKey` (`authorId`),
  KEY `bookKey` (`bookId`),
  CONSTRAINT `authorKey` FOREIGN KEY (`authorId`) REFERENCES `authors` (`id`),
  CONSTRAINT `bookKey` FOREIGN KEY (`bookId`) REFERENCES `books` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='links of authors and books';

-- Дамп данных таблицы books.author_of_book: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `author_of_book` DISABLE KEYS */;
REPLACE INTO `author_of_book` (`id`, `authorId`, `bookId`) VALUES
	(1, 1, 1);
/*!40000 ALTER TABLE `author_of_book` ENABLE KEYS */;

-- Дамп структуры для таблица books.books
CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `year` varchar(50) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `genre` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы books.books: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
REPLACE INTO `books` (`id`, `name`, `year`, `description`, `genre`) VALUES
	(1, 'Async & Performance', '2019', 'From the series You don`t know JS', 'IT');
/*!40000 ALTER TABLE `books` ENABLE KEYS */;

-- Дамп структуры для таблица books.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Дамп данных таблицы books.countries: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
REPLACE INTO `countries` (`id`, `name`) VALUES
	(1, 'Belarus'),
	(2, 'Russia'),
	(3, 'USA');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
